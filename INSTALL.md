***
ACG development toolkit

Copyright 2008-2024 INRIA

More information on "https://acg.loria.fr/"
License: CeCILL, see the LICENSE file or "http://www.cecill.info"
Authors: see the AUTHORS file
***

Installation instructions are available [here](https://acg.gitlabpages.inria.fr/dev/ACGtk/release/acgtk/users.html#install).
