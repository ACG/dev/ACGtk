#!/bin/sh

SRC="$1"
SEC="$2"

sed "/%%%SCRIPTING%%%/{
  s/%%%SCRIPTING%%%//
  r $SEC
}" "$SRC"
