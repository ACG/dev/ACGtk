{0 Scripting language}

{1 Introduction}

In this chapter, we describe the scripting language which comes with ACGtk.
This language, combined with the language of ACG grammars, is used to query
ACGtk to perform operations on the grammars and terms, such as parsing or
realisation. These queries are called commands.

Here is an example of a command:
{v
"lambda z. John (seeks (a (unicorn z))) : string" | parse type=Sentence lex_syn | realize lex_sem;
v}

There are three steps, called functions, executed by this command:
- The term [lambda z. John (seeks (a (unicorn z))) : string] is given to the
parse function.
- This function parses it in the lexicon [lex_syn] as a [Sentence] and sends
all resulting terms to the realize function.
- Then all these terms are realised in the lexicon [lex_sem] and the results
are displayed.

{1 Environment}

The environment of a scripting session contains ACG definitions (signatures and
lexicons). It is empty at the beginning of a session and some commands can
modify it.

{1 Functions}

Functions are the basic block of this scripting language. Some functions are
predefined, and it is also possible to define new functions by composing
existing functions.

As shown in the above example, most functions take as input a (possibly empty)
list of λ-terms and output an other list, which is meant to be given as input
to the next function. In addition, some functions take additional arguments,
such as a signature or lexicon name.

{2 Arguments}

{3 Syntax}

All arguments have a name and a type. In the following sections, this syntax is
used to describe the list of arguments of a function:
{v
f arg1:t1 arg2:t2 arg3=val3:t3
v}
This represents the example function [f], which takes 3 arguments, named [arg1],
[arg2] and [arg3], in this order. Their types are respectively [t1], [t2] and
[t3], and the third argument ([arg3]) has a default value ([val3]).

The arguments must be given to a function separated by spaces, and in the right
order, except if the name of the argument is explicitly written. Some arguments
may have a default value, so they can be omitted, and all the others argument
must be given.

For example, all these calls to the function [f] are equivalents:
{v
f arg1=val1 arg2=val2 arg3=val3
f val1 val2 val3
f val1 val2
f arg3=val3 arg1=val1 arg2=val2
f arg2=val2 val1
v}

For boolean arguments, there is an alternative syntax with [+] or [-] signs. If
[arg_bool] is a boolean argument of function [f], the two following calls are
equivalents and set the value of [arg_bool] to [true]:
{v
f arg_bool=true
f +arg_bool
v}
And these two calls are also equivalent, with the value of [arg_bool] set to
[false]:
{v
f arg_bool=false
f -arg_bool
v}

{3 Types}

The arguments of a function can have the following types:

- String ([string]): an example of literal value of type [string] is
["example"]. The quotes are optional when the string contains only
digits, letters, [-], [.] and [/], and mandatory otherwise.

- Integer ([int]): an example of literal value of type [int] is [230].

- Boolean ([bool]): there exists two possible values of type [bool]: [true]
and [false].

- ACG entry ([entry]): a valid value of type [entry] is the name of a lexicon
or a signature loaded in the environment.

- ACG lexicon ([lex]): a valid value of type [lex] is the name of a lexicon
loaded in the environment.

- ACG signature ([sig]): a valid value of type [sig] is the name of a signature
loaded in the environment. It is also possible to give a lexicon name where a
signature name is expected, and the abstract signature of the lexicon will be
used.

- ACG type ([type]): a valid value of type lex is a type written in ACG syntax,
for example [Sentence] or ["NP => (NP -> NP) -> VP"]. The quotes are optional
for atomic types, and mandatory in other cases.

If the type name is followed by a plus sign [+], it means that a list is
expected, with elements of this type separated by commas. For example, a valid
value of type [string+] is
["string with spaces", string_without_spaces, "third string"]. A list may contain
only one element, so ["one string"] is also a valid value of type [string+].

{2 List of predefined functions}

In this part, we list the predefined functions. When [\[terms\] |] is written
before a function description, it means that this function expects a list of terms as
input. When [| \[terms\]] is written after a function description, it means that
this function outputs a list of terms, which can be used as input of an other function,
with a [|], or just printed as result of the whole command.

%%%SCRIPTING%%%

{1 Commands}

There are two types of commands:
- Functions execution: this allows to execute one or more function,
composed by the pipe [|] operator.
- Function definition: this allows to assign a name to a new function which
won't be executed now. It is also possible to define a list of arguments which
will be given to the functions used inside the new defined function.

In the interactive mode, a command may end with a [;], but it is not mandatory.
In script mode, the [;] is always mandatory at the end of commands.

{2 Functions execution}

A command can execute one or more functions. If there are multiple functions in
a single command, they are composed by the pipe [|] operator, which gives the
list of λ-terms for a function to the following function.

For example, in this command:
{v
list-terms sig Sentence | realize lex
v}
There are two function calls, the first one is [list-terms sig Sentence], which
will output terms. These terms will be given as input to the second function
call, [realize lex], which will also output other terms. Since it is the last
function of the command, its output will be printed.

{3 Term literal}

A command may also start by a term literal. It will be given as input to the
next function of the command. There are two possible syntaxes:

- At the beginning of the command, followed by a pipe operator, for example:
{v
"Eat John (A Pizza) : S" | realize sig
v}
- At the end of the command, preceded by a "<" operator. This syntax is allowed
only when the command contains exactly one function. For example:
{v
realize sig < "Eat John (A Pizza) : S"
v}
These two commands are equivalent.

{2 Function definition}

It is possible to define custom functions by composing some other functions.
The custom functions can have parameters, to give to the functions used in its
definition.

Example of a function definition:
{v
let parse_real lex1 t lex2 n=2 := parse lex1 t | limit n | realize lex2;
v}

The parameters may have a default value, given with the operator "=" (in the
example, the parameter [n] has the default value [2]).

After the definition, it is possible to call it by its name (here [parse_real]):
{v
"lambda z. mary (loves (john z) : str" | parse_real string_lex sem_lex t=Sentence;
v}

{1 Examples}

In this section we give some examples of commands. These commands do not end with
a [;], to they are only valid in interactive mode. To use them in scripts, a [;]
must be added at the end of each command.

- To print the help:
{v
help
v}

- To load the file ["grammar.acgo"]:
{v
load grammar.acgo
v}

- To check if a term is correct in [sig]:
{v
"Loves John Mary : S" | check sig
v}

- To realize a term in the lexicon [lex]:
{v
"Seeks John (A Unicorn) : S" | realize lex
v}

- To parse a term in [lex1], and then realize all resulting terms in [lex2]:
{v
"lambda z. john (seeks (a (unicorn z))) : str" | parse lex1 Sentence | realize lex2
v}

- To generate a term of type [S] in [sig]:
{v
list-terms sig S | limit
v}

- To generate a list of 6 terms of type [S] in [sig] and realize them in [lex]:
{v
list-terms sig S | limit 6 | realize lex
v}

- To create a graph in "graph.svg" of the realization of the last term in lexicons [lex1] and [lex2]:
{v
last | limit | realize svg="graph.svg" lex1,lex2
v}

- To define a custom function to parse a term and realize all the resulting terms:
{v
let parse_real parse_lex parse_type real_lex := parse parse_lex parse_type | realize real_lex
v}
