##########################################################################
#                                                                        #
#                 ACG development toolkit                                #
#                                                                        #
#                  Copyright 2008-2024 INRIA                             #
#                                                                        #
#  More information on "https://acg.loria.fr/"                           #
#  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     #
#  Authors: see the AUTHORS file                                         #
#                                                                        #
#                                                                        #
#                                                                        #
##########################################################################

ACGtk_DIRNAME = $(shell basename `pwd`)
include mk/variables.mk

clean:
	dune clean
	find . -name "*~" -exec rm -f {} \;
.PHONY: clean

install:
	dune install
.PHONY: install

uninstall:
	dune uninstall
.PHONY: uninstall

tar: $(TAR_RELEASE).tar.gz
.PHONY: tar

PREVIOUS_DATES ?= $(shell echo $$(($$(date +"%Y") - 1)))
NEW_DATES ?= $(shell date +"%Y")

## update copyright date in files
copyright:
	find . -type f -not -wholename "*/tools/*" -not -wholename "*/.git/*" -not -wholename "*/_*" -exec grep -l 2008-$(PREVIOUS_DATES) {} \; | xargs -I file sed -i -e 's/2008-$(PREVIOUS_DATES)/2008-$(NEW_DATES)/' file
.PHONY: copyright

## Builds the archive
$(TAR_RELEASE).tar.gz:
	@if test -d ../$(TAR_RELEASE) ; then rm ../$(TAR_RELEASE) ; fi
	@cd .. && ln -s $(ACGtk_DIRNAME) $(TAR_RELEASE) && cd $(ACGtk_DIRNAME)
	@echo Building $(TAR_RELEASE).tar.gz
	@touch $(TAR_RELEASE).tar.gz	# create the file to avoid tar complaining file is changing as it reads it
	tar	\
		--exclude $(TAR_RELEASE).tar.gz	\
		--exclude conf-readline.opam \
		--exclude="*/.git*"	\
		--exclude "*/_build"	\
		--exclude "$(TAR_RELEASE)/tools"	\
		--exclude "$(TAR_RELEASE)/ACGtk-devtools"	\
		--exclude "$(TAR_RELEASE)/*.tar.*"	\
		--exclude "*~"	\
		-C ..	\
		-zcf $(TAR_RELEASE).tar.gz	\
		-h $(TAR_RELEASE) 
	@if test -d ../$(TAR_RELEASE) ; then rm ../$(TAR_RELEASE) ; fi

## Synchronizes the version number between all version files
version: src/utils/version.ml
.PHONY: version

## Synchronizes binary version number according to acgtk.version
src/utils/version.ml: acgtk.version
	printf "%s.%s\n" $$(cat acgtk.version) $$(date "+%Y-%m-%d") |	\
		xargs -I VERSION_NUMBER sed -i -e 's/version = "[^"]*"/version = "VERSION_NUMBER"/g' $@

# Generates a full opam file refering to the local tarball. Aims to be
# used for local opam repositories
opam: $(TAR_RELEASE).tar.gz
	sha512sum $< |	\
		cut -d " " -f 1 |	\
		xargs -I{}	\
		printf 'url {\n     src: "file://%s/%s"\n     checksum: "sha512=%s"\n}\n' $$(pwd) $$(basename $<) {}	\
		| cat acgtk.opam - >  $@

FORCE:
.PHONY: FORCE
