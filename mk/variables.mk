##########################################################################
#                                                                        #
#                 ACG development toolkit                                #
#                                                                        #
#                  Copyright 2008-2024 INRIA                             #
#                                                                        #
#  More information on "https://acg.loria.fr/"                           #
#  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     #
#  Authors: see the AUTHORS file                                         #
#                                                                        #
#                                                                        #
#                                                                        #
##########################################################################

## ACGtk_DIRPATH should be set depending of the Makefile calling variables
ifndef ACGtk_DIRPATH
ACGtk_DIRPATH = "."
endif

VERSION_BASENAME = $(shell cat "$(ACGtk_DIRPATH)/acgtk.version")
VERSION = $(VERSION_BASENAME)-$(shell date "+%Y%m%d")
TAR_RELEASE = acg-$(VERSION)
