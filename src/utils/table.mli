(**************************************************************************)
(*                                                                        *)
(*                 ACG development toolkit                                *)
(*                                                                        *)
(*                  Copyright 2008-2024 INRIA                             *)
(*                                                                        *)
(*  More information on "https://acg.loria.fr/"                     *)
(*  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     *)
(*  Authors: see the AUTHORS file                                         *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(**************************************************************************)

(** This module implements a functor returning an {e non-funtional}
    lookup table when provided with a given size *)

(** The signature module for modules providing size *)
module type BASE = sig
  val b : int
  (** The size value *)
end

(** The signature module for tables *)
module type TABLE = sig
  exception Not_found
  (** Exceptions raised by functions *)

  exception Conflict

  type 'a t
  (** The type of the tables *)

  type key
  (** The type of the key *)

  val empty : 'a t
  (** [empty] returns the empty table *)

  val add : ?overwrite:bool -> key -> 'a -> 'a t -> 'a t
  (** [add k v t] modifies the table [t] to add the element [v] at key
     [k]. The optional value [overwrite] is set to false by
     default. When set to false, the [add] function raises an
     exception {!exception:Conflict} when the key [k] was already
     associated with some value. When set to [true], the [add]
     function does not raise {!exception:Conflict} if some value was
     already associated to the key *)

  val find : key -> 'a t -> 'a
  (** [find k t] returns the element associated with the key [k] in
      [t]. Raises {!exception:Not_found} if no such element
      exists *)

  val fold : (key -> 'a -> 'b -> 'b) -> 'b -> 'a t -> 'b
  (** [fold f a t] returns [f kn vn (f kn-1 vn-1 (...(f k1 v1 a)
      ...))] where the [ki] and [vi] are the associated values in
      [t]. The elements are listed in order wrt. to the key *)

  val iter : (key -> 'a -> unit) -> 'a t -> unit
  (** [fold f a t] returns [f kn vn (f kn-1 vn-1 (...(f k1 v1 a)
      ...))] where the [ki] and [vi] are the associated values in
      [t]. The elements are listed in order wrt. to the key *)

  val pp :
    ?sep:
      ( (Format.formatter -> key * 'a -> unit) -> key * 'a -> unit,
        Format.formatter,
        unit,
        unit,
        unit,
        (Format.formatter -> key * 'a -> unit) -> key * 'a -> unit )
      format6 ->
    (Format.formatter -> key -> 'a -> unit) ->
    Format.formatter ->
    'a t ->
    unit
end

(** This modules provides the functor *)
module Make_table (_ : BASE) : TABLE with type key = int
