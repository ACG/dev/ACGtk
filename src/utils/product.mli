module type Elt = sig
  type t

  val to_string : t -> string
end

module Make (E : Elt) : sig
  type tuple
  type t

  val list_to_tuple : E.t list -> tuple
  val tuple_to_list : tuple -> E.t list (* tuples as list of elements *)
  val tuple_to_string : tuple -> string

  val product : int -> tuple -> t
  (** [product n t] returns the product [T×T×…×T] ([n] time) where [T]
      is the set of elements of [t] *)

  val fold : ('a -> tuple -> 'a) -> 'a -> t -> 'a
  (** [map f product] the list of [f t] where [t] ranges over the
     tuples of [product]. *)

  val to_list : t -> E.t list list (* all relations as list of tuples *)
  val to_string : t -> string
end
