type 'a t =
| Nil
| Cons of 'a * (unit -> 'a t)

let one e =
  Cons (e, fun () -> Nil)

let rec append ll1 f_ll2 =
  match ll1 with
  | Cons (e, f_ll1) -> Cons (e, fun () -> append (f_ll1 ()) f_ll2)
  | Nil -> f_ll2 ()

let rec append_mix ll1 f_ll2 =
  match ll1 with
  | Cons (e, f_ll1) -> Cons (e, fun () -> append_mix (f_ll2 ()) f_ll1)
  | Nil -> f_ll2 ()

let rec map f ll =
  match ll with
  | Cons (e, f_ll) -> Cons (f e, fun () -> map f (f_ll ()))
  | Nil -> Nil

let mapi f ll =
  let rec mapi_aux ll i =
    match ll with
    | Cons (e, f_ll) -> Cons (f e i, fun () -> mapi_aux (f_ll ()) (i + 1))
    | Nil -> Nil in
  mapi_aux ll 0

let rec iter f ll =
  match ll with
  | Cons (e, f_ll) -> f e; iter f (f_ll ())
  | Nil -> ()

let rec filter_map f ll =
  match ll with
  | Cons (e, f_ll) ->
    (match f e with
    | Some e -> Cons (e, fun () -> filter_map f (f_ll ()))
    | None -> filter_map f (f_ll ()))
  | Nil -> Nil

let rec fold_left f acc ll =
  match ll with
  | Cons (e, f_ll) -> fold_left f (f acc e) (f_ll ())
  | Nil -> acc

let rec fold_right f ll acc =
  match ll with
  | Cons (e, f_ll) -> f e (fold_right f (f_ll ()) acc)
  | Nil -> acc

let rec join ll_ll =
  match ll_ll with
  | Cons (ll, f_ll_ll) -> append ll (fun () -> join (f_ll_ll ()))
  | Nil -> Nil

let rec join_mix ll_ll =
  match ll_ll with
  | Cons (ll, f_ll_ll) -> append_mix ll (fun () -> join_mix (f_ll_ll ()))
  | Nil -> Nil

let rec bind_mix f ll =
  match ll with
  | Cons (e, f_ll) -> append_mix (f e) (fun () -> bind_mix f (f_ll ()))
  | Nil -> Nil

let rec from_list l =
  match l with
  | e :: l -> Cons (e, fun () -> from_list l)
  | [] -> Nil
