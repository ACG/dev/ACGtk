module ConfigState = struct
  type t = {
    use_magic_parsing : bool;
        (* the call to parsing functions use
           magic set rewriting if
           [use_magic_parsing] is [true] and
           use the regular datalog program
           resulting from reduction if it is
           false *)
  }

  let config = { use_magic_parsing = true }
  let set_magic_parsing b _c = { use_magic_parsing = b }
  let get_magic_parsing c = c.use_magic_parsing
end
