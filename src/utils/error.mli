type pos = Lexing.position * Lexing.position
val dummy_pos : pos

exception AcgtkError of string * (Format.formatter -> unit) * (pos option)

val base_prompt : string
val cont_prompt : string

module type ERROR_MANAGER =
  sig
    type t
    val kind : string
    val pp : Format.formatter -> t -> unit
  end

module type ERROR_HANDLER =
sig
  type manager
  val emit : ?loc:pos -> manager -> 'a
end

module ErrorManager (E : ERROR_MANAGER) : ERROR_HANDLER with type manager = E.t

val underline_error : string -> pos -> unit
val compute_comment_for_location : pos -> string

val set_position : Sedlexing.lexbuf -> Lexing.position -> unit
val quote_position : pos -> pos

val list_to_string : string list -> string

val print_error : exn -> string option -> unit
val print_error_fatal : exn -> string option -> 'a

val pp_text : Format.formatter -> string -> unit
(** [pp_text fmt msg] pretty prints [msg] on [fmt] replacing all
   spaces by formatted spaces "@ ".*)
