module ConfigState : sig
  type t = {
    use_magic_parsing : bool;
        (* the call to parsing functions use
           magic set rewriting if
           [use_magic_parsing] is [true] and
           use the regular datalog program
           resulting from reduction if it is
           false *)
  }

  val config : t
  (** [config] is a configuration state *)

  val set_magic_parsing : bool -> t -> t
  (** [set_magic_parsing b config] sets the [use_magic_parsing] field of [config] to [b]. *)

  val get_magic_parsing : t -> bool
end
