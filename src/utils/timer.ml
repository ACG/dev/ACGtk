type t = Mtime_clock.counter
let top = Mtime_clock.counter
let elapsed fmt c =
  Mtime.Span.pp fmt (Mtime_clock.count c)
let diff fmt (c1, c2) =
  Mtime.Span.(pp fmt (abs_diff (Mtime_clock.count c1) (Mtime_clock.count c2)))

module Log = Xlog.Make (struct
    let name = "Timer"
  end)


let msg = Log.msg
let app = Log.app
let err = Log.err
let warn = Log.warn
let info = Log.info
let debug = Log.debug

let set_level l = Logs.Src.set_level Log.src (Some l)
