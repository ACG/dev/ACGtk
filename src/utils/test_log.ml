module TestLog = struct
  let src = Logs.Src.create "Test Log"

  module Log = (val Logs.src_log src)
  include Log
end

let it_nbr = 1000000000
let rec test_fct i = if i <= 0 then i else test_fct (i - 1)

let to_us span =
  let float_ns_span = Mtime.Span.to_float_ns span in
  let float_ns_unit = Mtime.Span.(to_float_ns ns) in
  let float_span = float_ns_span /. float_ns_unit in
  float_span /. 1000.0

let test_fct _level =
  let () = TestLog.msg _level (fun m -> m "Ça commence…") in
  let res = test_fct it_nbr in
  let () = TestLog.msg _level (fun m -> m "C'est fini.") in
  res

let wrap f =
  let c = Mtime_clock.counter () in
  (*  let () = TestLog.app (fun m -> m "Starts wrapping...") in *)
  let () =
    assert (
      let () =
        TestLog.debug (fun m ->
            m "Test function ran and reached %d" (f Logs.Info))
      in
      true)
  in
  let span = Mtime_clock.count c in
  (*  TestLog.app (fun m -> m "Ends (%a)." Mtime.Span.pp span)  *)
  to_us span

let average iter f arg =
  let rec average_aux i acc =
    if i <= 0 then acc
    else
      let () =
        if i mod 100 = 0 then Logs.info (fun m -> m "Itération %d" i) else ()
      in
      average_aux (i - 1) (acc +. f arg)
  in
  let () = Logs.app (fun m -> m "Début calcul de moyenne.") in
  Logs.app (fun m ->
      m "Moyenne sur %i itérations : %9.2f µs" iter
        (average_aux iter 0.0 /. float_of_int iter))

let () =
  let () =
    UtilsLib.Utils.pp_list Format.pp_print_int Format.std_formatter [ 1; 2; 3 ]
  in
  let () =
    UtilsLib.Utils.pp_list ~sep:",@\n" Format.pp_print_int Format.std_formatter
      [ 1; 2; 3 ]
  in

  let iter = 10 in
  let () = Fmt_tty.setup_std_outputs ~style_renderer:`Ansi_tty () in
  let () = Logs.set_level ~all:true (Some Logs.Debug) in
  let () = Logs.set_reporter (Logs_fmt.reporter ()) in
  let () = TestLog.app (fun m -> m "Niveau Debug") in
  let () = average iter wrap test_fct in
  let () = TestLog.app (fun m -> m "Niveau Info") in
  let () = Logs.set_level ~all:true (Some Logs.Info) in
  let () = average iter wrap test_fct in
  ()
