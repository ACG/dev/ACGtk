type 'a t =
| Nil
| Cons of 'a * (unit -> 'a t)

val one : 'a -> 'a t

val append : 'a t -> (unit -> 'a t) -> 'a t
val append_mix : 'a t -> (unit -> 'a t) -> 'a t

val map : ('a -> 'b) -> 'a t -> 'b t
val mapi : ('a -> int -> 'b) -> 'a t -> 'b t

val iter : ('a -> unit) -> 'a t -> unit

val filter_map : ('a -> 'b option) -> 'a t -> 'b t

val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a
val fold_right : ('b -> 'a -> 'a) -> 'b t -> 'a -> 'a

val join : 'a t t -> 'a t
val join_mix : 'a t t -> 'a t

val bind_mix : ('a -> 'b t) -> 'a t -> 'b t

val from_list : 'a list -> 'a t
