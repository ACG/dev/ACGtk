(**************************************************************************)
(*                                                                        *)
(*                 ACG development toolkit                                *)
(*                                                                        *)
(*                  Copyright 2008-2024 INRIA                             *)
(*                                                                        *)
(*  More information on "https://acg.loria.fr/"                     *)
(*  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     *)
(*  Authors: see the AUTHORS file                                         *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(**************************************************************************)

(** This module provides some useful modules or functions *)

module StringSet : Set.S with type elt = String.t
(** [StringSet] is a module for sets of [strings] *)

module StringMap : Map.S with type key = String.t
(** [StringMap] is module for maps from [strings] to type ['a] *)

module IntMap : Map.S with type key = int
(** [IntMap] is a module for maps from [int] to type ['a] *)

module IntSet : Set.S with type elt = int
(** [IntSet] is a module for sets of [int] *)

val string_of_list : string -> ('a -> string) -> 'a list -> string
(** [string_of_list sep to_string [a_1;...;a_n]] returns a string made
    of the strings [to_string a_1] ... [to_string a_n] concatenated with
    the separator [sep] between each of the elements (if the list is of
    length greater than 2) *)

val pp_list :
  ?sep:(unit, Format.formatter, unit, unit, unit, unit) format6 ->
  ?terminal:(unit, Format.formatter, unit, unit, unit, unit) format6 ->
  (Format.formatter -> 'a -> unit) ->
  Format.formatter ->
  'a list ->
  unit
(** [pp_list ~sep ppf fmt l] pretty prints on the formater [fmt] the
   items of the list [l], each of them being pretty-printed by
   [ppf]. Example usage: [pp_list ~sep:",@\n" Format.pp_print_int
   Format.std_formatter [1;2;3] ] *)

val pp_list_i :
  ?sep:(unit, Format.formatter, unit, unit, unit, unit) format6 ->
  ?terminal:(unit, Format.formatter, unit, unit, unit, unit) format6 ->
  (Format.formatter -> (int * 'a) -> unit) ->
  Format.formatter ->
  'a list ->
  unit
(** [pp_list_i ~sep ppf fmt l] pretty prints on the formater [fmt] the
   items of the list [l], each of them, together with its position
   (starting at 1) being pretty-printed by [ppf].*)



val pp_text : Format.formatter -> string -> unit
(** [pp_text fmt t] pretty prints on the formater [fmt] the text of
   of the string [t]. *)

val intersperse : 'a -> 'a list -> 'a list
(** [intersperse sep [a_1;...;a_n]] returns a list where elements of
    the input list are interspersed with [sep] as in [a_1; sep; a_2;
    sep; ...; sep; a_n]. *)

val cycle : int -> 'a list -> 'a list
(** [cycle n xs] returns the first [n] elements of the infinite list formed
    by cyclically repeating the elements of [xs]. Returns the empty list if
    [xs] is empty. *)

val fold_left1 : ('a -> 'a -> 'a) -> 'a list -> 'a
(** [fold_left1 plus elems] sums up the elements in [elems] using [plus]. A
    generalization of List.fold_left from monoids to semigroups, where we
    don't have any neutral element. Assumes [elems] is non-empty. *)

val resize_terminal : width:int option -> unit
(** [resize_ terminal ~width] resets the formatter width to [w] if
    [width] is [Some w], and computes its value automatically
    otherwise (depending on whether [stdout] is a tty or not, i.e., is
    a file. In that case, the width is set ton [max_int -1] *)


val no_pp : unit -> unit
val fformat : Format.formatter -> ('a, Format.formatter, unit) format -> 'a
val format : ('a, Format.formatter, unit) format -> 'a
val sformat : ('a, Format.formatter, unit) format -> 'a

val format_of_list :
  Format.formatter -> string -> ('a -> string) -> 'a list -> unit

(*
val blue : string -> string
val red : string -> string
val green : string -> string
val yellow : string -> string
val white : string -> string

val color_fun : string -> string
val color_sig : string -> string
val color_lex : string -> string
*)
  
val bold_pp : Format.formatter -> string -> unit
val blue_pp : Format.formatter -> string -> unit
val red_pp : Format.formatter -> string -> unit
val green_pp : Format.formatter -> string -> unit
val magenta_pp : Format.formatter -> string -> unit
val yellow_pp : Format.formatter -> string -> unit

val fun_pp : Format.formatter -> string -> unit
val sig_pp : Format.formatter -> string -> unit
val lex_pp : Format.formatter -> string -> unit
val terms_pp : Format.formatter -> string -> unit
val binary_pp : Format.formatter -> string -> unit


val string_of_list_rev : string -> ('a -> string) -> 'a list -> string
(** [string_of_list_rev sep to_string [a_1;...;a_n]] returns a string
    made of the strings [to_string a_n] ... [to_string a_1]
    concatenated with the separator [sep] between each of the elements
    (if the list is of length greater than 2) *)

val find_file : string -> string list -> Error.pos -> string
(** [find_file f dirs] tries to find a file with the name [f] in the
    directories listed in [dirs]. If it finds it in [dir], it returns
    the full name [Filename.concat dir f]. To check in the current
    directory, add [""] to the list. It tries in the directories of
    [dirs] in this order and stops when it finds such a file. If it
    can't find any such file, raise the exception [No_file(f,msg)]
    where [msg] contains a string describing where the file [f] was
    looked for.*)

val ( >> ) : ('b -> 'c) -> ('a -> 'b) -> 'a -> 'c

(** This function is useless
                                         when Bolt log level is
                                         NONE *)

(*val log_iteration : (string -> unit) -> string -> unit  *)

val decompose : input:int -> base:int -> int list
(** [decompose ~input:i ~base:b] returns the decomposition of [i] in
   the base [b] as a list of integers (between [0] and [b]). *)
