module AcgEnv = AcgData.Environment.Environment

module Log:UtilsLib.Xlog.MyLOG

type file_type =
  | Object
  | Data

val load_env : with_magic:bool -> string -> string list -> AcgEnv.t -> UtilsLib.Error.pos -> file_type * AcgEnv.t
(** [load_env ~with_magic filename dirs env] loads the environment
   contained in the file [filename] (searched in directories [dirs])
   and adds it to [env].  The file can be an object file or an regular
   acg data file, and the function returns the new environment and the
   type of the file. If [with_magic] is set to true, when parsing acg
   data files, magic rewritting is enabled*)

val save_env : force:bool -> string -> AcgEnv.t -> unit
(** [write ~force filename env] writes the enviromnent [env] into the
   file [filename] as an object. If [force] is set to [true], all the
   data of the environment are dumped.*)
