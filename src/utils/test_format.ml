
open UtilsLib

let args =
  [ "toto";
    "titi";
    "tutu";
    "tata";
    "tonton"
  ]

let stars () = Format.printf "%s@." (String.make 10 '*')

let () = Format.set_margin 17

let () = Utils.pp_list ~sep:"@ " ~terminal:"@." Format.pp_print_string Format.std_formatter args

let () = stars ()

let () = Format.(printf "@[<hov>%a@]" (Utils.pp_list ~sep:"@ " ~terminal:"@." Format.pp_print_string) args)
let () = stars ()

let () = Format.(printf "@[<v>%a@]" (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@;") pp_print_string) args)
let () = stars ()


let () =
  let () = Format.printf "@[<v>" in
  let () = Format.printf ("toto" ^^ "@;") in
  let () = Format.printf ("titi" ^^ "@;") in
  let () = Format.printf ("tutu" ^^ "@;") in
  let () = Format.printf ("tata" ^^ "@;") in
  let () = Format.printf ("tonton" ^^ "@;") in
      Format.printf "@]"

let () = stars ()
let () =
  let () = Format.printf "@[<v>@[" in
  let () = Utils.pp_list ~sep:"A@.A" Format.pp_print_string Format.std_formatter args in
      Format.printf "@]@]"

let () = stars ()
