module E = struct
  type t = int

  let to_string = string_of_int
end

module P = UtilsLib.Product.Make (E)

let print_result msg p =
  let () = Printf.printf "Le résultat pour %s est\n%!" msg in
  let () = Printf.printf "%s\n%!" (P.to_string p) in
  Printf.printf "**********************************************\n"

let values = P.list_to_tuple [ 0; 1 ]
let one_ary_uple = P.product 1 values
let () = print_result "1-uple" one_ary_uple
let two_ary_uple = P.product 2 values
let () = print_result "2-uple" two_ary_uple
let three_ary_uple = P.product 3 values
let () = print_result "3-uple" three_ary_uple
let five_ary_uple = P.product 5 values
let () = print_result "5-uple" five_ary_uple
