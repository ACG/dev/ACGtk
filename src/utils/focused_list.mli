module Focused_list : sig
  type 'a t =
    | List_zip of ('a list * 'a * 'a list)
        (** This type aims to implement a zipper for lists. The context is
      the first parameter. It represents the list of the elements {e
      in reverse order} that has been traversed to reach the focused
      element (the second parameter). The last element is the
      remaining elements of the list. *)

  exception Empty_list
  exception End_of_list

  val init : 'a list -> 'a t
  (** [init l] inits a focused list. It raises an exception
     {!Empty_list} if [l] is empty, hence has no element to focus
     on. *)

  val forward : ?step:int -> 'a t -> 'a t
  (** [forward z] returns a the new focused_list where the focused
     element is the [step] steps (default to 1) next to the current
     one in the initial list. It raises {!End_of_list} if no such
     element is available *)

  val backward : 'a t -> 'a t
  (** [backward z] returns a the new focused_list where the focuses
      element is the previous one in the initial list. It raises
      {!End_of_list} if no such element is available *)

  val fold : ('b -> 'a list * 'a * 'a list -> 'b) -> 'b -> 'a t -> 'b
  (** [fold f a z] returns [f (... (f (f a z1) z2) ...) z_n] where
      [z_1=z] and [z_{i+1}=forward z_i] and [forward z_n] would raise
      an exception {!End_of_list}. *)

  val fold_forward : ?include_focus:bool -> ('b -> 'a -> 'b) -> 'b -> 'a t -> 'b
  (** [fold_forward ~include_focus f a z] returns [f (... (f (f a z1) z2) ...) z_n] where
     [z_1=focus z] if [include_focus] is [true] (default) and
     [z_1=focus (forward z)] otherwise (returns [a] if [forward z]
     raises [End_of_list]) and [z_{i+1}=focus (forward z_i)] and
     [forward z_n] would raise an exception {!End_of_list}. *)

  val focus : 'a t -> 'a
  (** [focus l] returns the focus element of the focused list [l] *)

  val zip : 'a t -> 'a list
end
