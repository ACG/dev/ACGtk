(** A
   {{:https://erratique.ch/software/logs/doc/Logs/index.html#module-type-LOG}
   Logs.LOG} module that includes its source *)

module type MyLOG = sig
  include Logs.LOG

  val src : Logs.src
end

val stamp : Mtime_clock.counter -> Logs.Tag.set

val log_pairs :
  src:Logs.src ->
  Logs.level ->
  ((('a -> 'b -> 'c, Format.formatter, unit, unit) format4 ->
   ('d -> 'a) * 'd ->
   ('e -> 'b) * 'e ->
   'c) ->
  unit) ->
  unit

val set_level : app:string -> ?colored:bool -> Logs.level -> unit
(** [set_level ~app l] sets the log level to [l] and provides the name
    [app] to the default application log. *)

module Make (_ : sig
  val name : string
end) : MyLOG
