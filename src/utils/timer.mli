type t
val top : unit -> t
val elapsed : Format.formatter -> t -> unit
val diff : Format.formatter -> t * t -> unit

val msg: Logs.level -> 'a Logs.log
val app: 'a Logs.log
val err: 'a Logs.log
val warn: 'a Logs.log
val info: 'a Logs.log
val debug: 'a Logs.log
    
val set_level: Logs.level -> unit

  
