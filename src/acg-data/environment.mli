(**************************************************************************)
(*                                                                        *)
(*                 ACG development toolkit                                *)
(*                                                                        *)
(*                  Copyright 2008-2024 INRIA                             *)
(*                                                                        *)
(*  More information on "https://acg.loria.fr/"                     *)
(*  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     *)
(*  Authors: see the AUTHORS file                                         *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(**************************************************************************)
open Signature
open Acg_lexicon

(** This modules implements an environment containing signatures and
   lexicons. It relies on the Signature.Data_Signature and the modules
   Acg_lexicon.Data_Lexicon *)

(** This module signature describes the interface for modules implementing environments *)
module Environment : sig
  type t
  (** The type of the environment *)
  
  type dumped_t
  (** The type of the environment when dumped to file *)

  type entry = Signature of Data_Signature.t | Lexicon of Data_Lexicon.t
  (** The type of what an environment can contain *)

  val empty : t
  (** [empty] is the empty environmnent *)

  val insert : ?overwrite:bool -> entry -> to_be_dumped:bool -> t -> t
  (** [insert c ~to_be_dumped:d e] adds the content [c] into the
     environment [e] and returns the resulting environmnent. If [d] is
     set to true, then [c] will be dumped in [e] . The default value
     for the [overwrite] optional parameter is [false].*)

  val list_signatures : t -> string list
  (** [list_signatures e] returns a list containing the names of all
     signatures in the environment [e]. *)

  val list_lexicons : t -> string list
  (** [list_lexicons e] returns a list containing the names of all
     lexicons in the environment [e]. *)

  val prepare_dump : ?force:bool -> filename:string -> t -> dumped_t
  (** [prepare_dump ~filename e] returns the environment a ready to be
     dumped (in file [filename] environment of type {!dumped_t}
     similar to [e], but without the data which has [false] in
     [to_be_dumped]. The data of the returned environment has [false]
     in [to_be_dumped]. If [force] is set to [true] (default is
     [false]), all the data available in the environement are dumped
     anyway, whether they have their [to_be_dumped] flag set to [true]
     or not. It is useful in case we want to generate a full
     environment out of only object files. *)

  val get : string -> t -> UtilsLib.Error.pos -> entry
  (** [get name e loc] returns the entry of name [name] in the
     environment [e]. Raise a corresponding error is there is no such
     entry. *)

  val get_opt : string -> t -> entry option
  (** [get_opt name e] returns the entry of name [name] in the
     environment [e]. Returns none if there is no such entry. *)

  val get_signature : string -> t -> UtilsLib.Error.pos -> Data_Signature.t
  (** [get name e loc] returns the signature of name [name] in the
     environment [e]. Raise a corresponding error is there is no such
     signature. *)

  val get_lexicon : string -> t -> UtilsLib.Error.pos -> Data_Lexicon.t
  (** [get name e loc] returns the lexicon of name [name] in the
     environment [e]. Raise a corresponding error is there is no such
     lexicon. *)

  val append : ?overwrite:bool -> t -> dumped_t -> t
  (** [append e1 e2] merges the two environment [e1] and [e2]. If an
     entry appears in both environment then the one of [e2] is kept if
     the [overwrite] parameter is set to [true] (default is
     [false]). If set to [false], if an entry appears in both
     environment, an error is emitted. *)

  val iter : (entry -> unit) -> t -> unit
  (** [iter f e] applies f to every data contained in the environment
     *)

  val fold : (entry -> 'a -> 'a) -> 'a -> t -> 'a
  (** [fold f a e] returns [f a_n (f a_n-1 (... (f a1 (f a0 a))
      ... ))] where the [a_0 ... a_n] are the [n+1] elements of the
      environmnent *)

  val sig_number : t -> int
  (** [sig_number e] returns the number of signatures an environment
      contains *)

  val lex_number : t -> int
  (** [sig_number e] returns the number of lexicons an environment
      contains *)

  val choose_signature : t -> Data_Signature.t option
  (** [choose_signature e] returns a randomly chosen signature in the
      environment [e] *)

  val magic : t -> t
  (** [magic env] returns a new env where all the magic programs for
        all the lexicons have been created *)

end
