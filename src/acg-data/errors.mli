(** This module contains the type of errors linked to entries in environments. *)
module Environment_l : sig
  type t =
    | EntryNotFound of string
    | NotALexicon of string
    | NotASignature of string
    | DuplicatedEntry of string
    | Other

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module EnvironmentErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Environment_l.t -> 'a
end

(** This module contains the type of errors linked to lexicons. *)
module Lexicon_l : sig
  type t =
    | MissingInterpretations of string * string * string list
    | BadInterpretationOfMacro of string * string * string
    | NotComposable of string * string * string
     (** [NotComposable (l, l1, l2)] is raised whenever
         lexicon [l] is defined using the composition [l2∘l1] at
         position [pos] but the abstract vocabulary of [l2] is not the
         object one of [l1]. *)
    | UnavailableSignature of (string * string * string)
    (** [UnavailableSignature (lex_name, sig_name and file_name)] is
       raised when a lexicon that depends on signature [sig_name] is
       loaded, but theire is no signature with this name in the
       current environment *)
    | NotCompatibleSignature of (string * string * string * string)
    (** [NotCompatibleSignature (lex_name, lex_filename, sig_name and
       sig_filename)] is raised when a lexicon that depends on
       signature [sig_name], defined in file [sig_filename], is
       loaded, and there actually is a signature with name [sig_name]
       in the environment, but it is not the one against which
       [lex_name] was compiled. *)

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module LexiconErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Lexicon_l.t -> 'a
end

(** This module contains the type of errors linked to types in lambda-terms. *)
module Type_l : sig
  type t =
    | AlreadyDefinedVar of string
    | NotDefinedVar of string
    | NotDefinedConst of string
    | NotWellTypedTerm of string * string
    | NotWellTypedTermPlus of string * string * string
    | NotWellKindedType of string
    | NonLinearVar of string
    | LinearVar of string
    | IsUsed of string * string
    | TwoOccurrencesOfLinearVariable of UtilsLib.Error.pos
    | NonEmptyContext of
        string
        * UtilsLib.Error.pos
        * UtilsLib.Error.pos
        * string
    | NotNormal
    | VacuousAbstraction of string * UtilsLib.Error.pos
    | Other

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module TypeErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Type_l.t -> 'a
end

module Cmd_l :
sig
  type t =
    | TypeMismatch of (Format.formatter -> unit) * (Format.formatter -> unit) * (Format.formatter -> unit) * (Format.formatter -> unit)
    (* (object type,
       abstract distinguished type,
       interpretation of abstract distinguished type,
       lexicon name *)
  val kind : string
  val pp : Format.formatter -> t -> unit
end

module CmdErrors : UtilsLib.Error.ERROR_HANDLER with type manager = Cmd_l.t
