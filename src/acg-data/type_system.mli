(**************************************************************************)
(*                                                                        *)
(*                 ACG development toolkit                                *)
(*                                                                        *)
(*                  Copyright 2008-2024 INRIA                             *)
(*                                                                        *)
(*  More information on "https://acg.loria.fr/"                     *)
(*  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     *)
(*  Authors: see the AUTHORS file                                         *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(**************************************************************************)

open Logic.Abstract_syntax
open Logic.Lambda

module Log : UtilsLib.Xlog.MyLOG
(** [Log] is the log module for Type_system *)

module type SIG_ACCESS = sig
  exception Not_found

  type t

  val unfold_type_definition : int -> t -> Lambda.stype
  (** [unfold_type_definition id t] returns the actual type for the
      type defined by [id] as the identifier of a type definition in
      the signature [t]. Fails with "Bug" if [id] does not correspond
      to a type definition *)

  val expand_type : Lambda.stype -> t -> Lambda.stype
  val find_term : string -> t -> Lambda.term * Lambda.stype
  val pp_type : t -> Format.formatter -> Lambda.stype -> unit
  val pp_term : t -> Format.formatter -> Lambda.term -> unit
end

module Type_System : sig
  module Make (Signature : SIG_ACCESS) : sig
    val typecheck :
      Abstract_syntax.term -> Lambda.stype -> Signature.t -> Lambda.term * bool
  end
end
