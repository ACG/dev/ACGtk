type config = {
  dirs : string list;
  rendering_config : SvgLib.Svg.config;
  step_by_step : bool;
  with_magic : bool;
  width : int option;
}
