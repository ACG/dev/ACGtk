let () =
	let () = Format.pp_set_max_indent Format.std_formatter 100000 in
	let fun_list = Scripting.Functions.def_fun_list None in
	Format.fprintf Format.std_formatter "%a" Scripting.Environment.doc_pp fun_list
