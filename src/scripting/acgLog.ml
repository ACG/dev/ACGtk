let set_acg_log () =
  (* setting UtilsLib modules *)
  let () = Logs.Src.set_level Containers.SharedForest.Log.src (Some Logs.Warning) in
  let () = Logs.Src.set_level UtilsLib.IdGenerator.Log.src (Some Logs.Warning) in
  (* setting Logic modules *)
  let () = Logs.Src.set_level Logic.TypeInference.Log.src (Some Logs.Warning) in
  let () = Logs.Src.set_level Logic.VarUnionFind.Log.src (Some Logs.Warning) in
  (* setting AcgData modules *)
  let () = Logs.Src.set_level AcgData.Acg_lexicon.Log.src (Some Logs.Warning) in
  let () = Logs.Src.set_level AcgData.Acg_lexicon.ParsingLog.src (Some Logs.Info) in
  let () = Logs.Src.set_level AcgData.Acg_lexicon.ParsingLog.src (Some Logs.Warning) in
  let () = Logs.Src.set_level AcgData.Reduction.Log.src (Some Logs.Warning) in
  let () = Logs.Src.set_level AcgData.Type_system.Log.src (Some Logs.Warning) in
  let () = Logs.Src.set_level AcgData.Signature.Log.src (Some Logs.Warning) in
  (* setting DatalogLib modules *)
  let () =
    Logs.Src.set_level DatalogLib.UnionFind.Log.src (Some Logs.Warning)
  in
  let () =
    Logs.Src.set_level DatalogLib.Datalog_AbstractSyntax.Log.src
      (Some Logs.Warning)
      (*(Some Logs.Info) *)
  in
  let () = Logs.Src.set_level DatalogLib.Datalog.Log.src (Some Logs.Warning) in
  (* setting MagicRewriting modules *)
  let () =
    Logs.Src.set_level MagicRewriting.Rewriting.Log.src
      (*(Some Logs.Debug)*) (Some Logs.Warning)
  in
  let () =
    Logs.Src.set_level MagicRewriting.Unique_binding.Log.src (*(Some Logs.Debug) *)
      (Some Logs.Warning)
  in
  (* setting Grammars modules *)
  let () =
    Logs.Src.set_level Grammars.Term_sequence.Log.src (Some Logs.Warning)
  in
  ()
