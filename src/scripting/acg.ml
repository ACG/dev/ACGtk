(**************************************************************************)
(*                                                                        *)
(*                 ACG development toolkit                                *)
(*                                                                        *)
(*                  Copyright 2008-2024 INRIA                             *)
(*                                                                        *)
(*  More information on "http://acg.gforge.inria.fr/"                     *)
(*  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     *)
(*  Authors: see the AUTHORS file                                         *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*  $Rev::                              $:  Revision of last commit       *)
(*  $Author::                           $:  Author of last commit         *)
(*  $Date::                             $:  Date of last commit           *)
(*                                                                        *)
(**************************************************************************)

open Cmdliner
open Scripting

let welcome_string = "Welcome to the ACG toplevel"
let version_string = Printf.sprintf "Version %s" UtilsLib.Version.version
let copyright_string = "©INRIA 2008-2024"

let bug_string =
  "Please send your comments or bug reports or feature requests to \
   sylvain.pogodalla@inria.fr"

type color_mode =
  | C_auto
  | C_yes
  | C_no

let welcome_msg () =
  let l =
    let l = Format.get_margin () in
    if l > 1000 then String.length bug_string + 10 else l
  in
  let _ = Format.flush_str_formatter () in
  let () = Format.printf "@[<v>" in
  let () =
    List.iter
      (fun s ->
        let prepend =
          try String.make ((l - String.length s) / 2) ' '
          with Invalid_argument _ -> ""
        in
        Format.printf "@[<h>%s%s@]@," prepend s)
      [ welcome_string; version_string; copyright_string; bug_string ]
  in
  Format.printf "@,@[<v>@[Type@ \"help\"@ to@ get@ help.@]@]@,@]@.@?"

let interactive env =
  let () = Sys.catch_break true in
  let () = Readline.set_word_break_chars " \t\n\"\\'`@$><=;|&{(," in
  let () =
    Readline.init ~history_file:(Sys.getenv "HOME" ^ "/.acg_history") ~program_name:"acg" ~catch_break:true () in
  let rec interactive_rec env =
    match Interpreter.interactive env with
    | Interpreter.Continue new_env ->
        interactive_rec new_env
    | Interpreter.Continue_Error -> interactive_rec env
    | Interpreter.Stop -> ()
  in
  interactive_rec env

let script env filename =
  let fullname = UtilsLib.Utils.find_file filename env.Environment.config.Config.dirs UtilsLib.Error.dummy_pos in
  let in_ch = In_channel.open_text fullname in
  let lexbuf = Sedlexing.Utf8.from_channel in_ch in
  let () = Sedlexing.set_filename lexbuf filename in
    Interpreter.script lexbuf env

let script_stdin env =
  let lexbuf = Sedlexing.Utf8.from_channel stdin in
  let () = Sedlexing.set_filename lexbuf "<stdin>" in
  Interpreter.script lexbuf env

let start term_width dirs with_magic rendering_config_o force_interactive files step_by_step seed color_mode build_info debug_mode =
  let () = if debug_mode then Printexc.record_backtrace true in
  let () = UtilsLib.Utils.resize_terminal ~width:term_width in
  let colors =
    match color_mode with
    | C_auto -> !ANSITerminal.isatty Unix.stdout
    | C_yes -> true
    | C_no -> false in
  let () = UtilsLib.Xlog.set_level ~app:"acg" ~colored:colors Logs.Warning in
  let () = AcgLog.set_acg_log () in
  let () = if debug_mode then Logs.Src.set_level AcgData.Acg_lexicon.ParsingLog.src (Some Logs.Info) in
  match build_info, SvgLib.Svg.has_cairo  with
  | true,true -> Logs.app (fun m -> m "Built@ with@ svg@ support,@ \
                                       provided@ by@ the@ cairo2@ \
                                       opam@ package.")
  | true,false -> Logs.app (fun m -> m "Built@ with@ no@ svg@ \
                                        support.@ To@ enable@ it,@ \
                                        install@ the@ cairo2@ package@ \
                                        with@ 'opam install cairo2'.")
  | false,_ ->
     let () =
       match seed with
       | Some s -> Random.init s
       | None -> Random.self_init () in
     let dirs = match dirs with [ "" ] -> dirs | _ -> "" :: dirs in
     let rendering_config = SvgLib.Svg.get_config ~filename:rendering_config_o ~dirs in
     let config = Config.{ dirs; rendering_config ; step_by_step ; with_magic ; width = term_width } in
     let env =
       Environment.
       {
         config;
         acg_env = AcgData.Environment.Environment.empty;
         functions = Functions.def_fun_list (Some config);
         last_value = None;
       }
     in

     let () =
       if not (!ANSITerminal.isatty Unix.stdin) && step_by_step then
         (Logs.set_reporter (Logs.format_reporter ()); (* Workaround to print the message on stderr, so that the user can see it in the terminal even if stdout is redirected to a file or a pipe. *)
          Logs.err (fun m -> m "Can't@ start@ in@ step@ by@ step@ mode,@ because@ the@ standard@ input@ channel@ does@ not@ correpond@ to@ a@ tty.");
          exit 1) in

     let interactive_requested = ((files = [] && !ANSITerminal.isatty Unix.stdin) || force_interactive) in
     let interactive_allowed = !ANSITerminal.isatty Unix.stdout && !ANSITerminal.isatty Unix.stdin in

     let () =
       if interactive_requested && interactive_allowed
       then
         welcome_msg ()
       else
         () in
     
     let env =
       try
         let env =
           List.fold_left script env files
         in
         
         let env =
           if not (!ANSITerminal.isatty Unix.stdin) then
             script_stdin env
           else
             env in
         env
       with
       | e -> UtilsLib.Error.print_error_fatal e None in
     
     if interactive_requested then
       (if interactive_allowed then
          interactive env
        else
          (Logs.set_reporter (Logs.format_reporter ()); (* Workaround to print the message on stderr, so that the user can see it in the terminal even if stdout is redirected to a file or a pipe. *)
           Logs.err (fun m -> m "Can't@ start@ in@ interactive@ mode,@ because@ the@ standard@ output@ channel@ or@ the@ standard@ input@ channel@ does@ not@ correpond@ to@ a@ tty.")))
  
  let start_t =
    let doc =
      "Interactive ACG command interpreter. Also parse and interpret files given \
       on the command line (if any)."
    in
  let term_width =
    let doc = "Set the terminal width to be used for pretty printing \
               output. When not set, the value is automatically set to \
               the size of the terminal if the standard ouput is a \
               terminal, or to max_int if the output is a file." in
    Arg.(value & opt (some int) None & info [ "w"; "terminal-width" ] ~doc)
  in
  let dirs =
    let doc =
      "Sets $(docv) as a directory in which file arguments can be looked for."
    in
    Arg.(value & opt_all dir [ "" ] & info [ "I"; "include" ] ~docv:"DIR" ~doc)
  in
  let with_magic =
    let doc = "Toggle on using magic set rewritten programs for \
               parsing (experimental feature). When set, parsing \
               commands using magic rewritten programs (if available \
               in object files generated by $(b,acgc))." in
    Arg.(value & flag & info [ "m"; "magic" ] ~doc) (* flag defaults
                                                       to false when
                                                       non present *)
  in
  let force_interactive =
    let doc = "Starts interactive mode even if script files are provided." in
    Arg.(value & flag & info [ "i"; "interactive" ] ~doc)
  in
  let step_by_step =
    let doc = "Executes scripts step by step. This means that the execution will
    be paused before running each command, which will be displayed, and after
    printing the result of commands which return terms. Also, this will print
    the executed script during the execution. This option also enable displaying
    the number of terms returned by such commands." in
    Arg.(value & flag & info [ "s"; "step-by-step" ] ~doc)
  in
  let seed =
    let doc = "Seed to use for initialization of the random number generator. If
    this paramenter is not provided, the random number generator will be
    initialized with a random seed." in
    Arg.(value & opt (some int) None & info [ "r"; "seed" ] ~doc)
  in
  let colors =
    let doc = "Controls the colors in output. Use $(b,yes) to enable colors,
    $(b,no) to disable them, or $(b,auto) to enable them if the output in a TTY." in
    let e = Arg.enum [ "auto", C_auto ; "yes", C_yes ; "no" , C_no ] in
    Arg.(value & opt e C_auto  & info [ "c"; "colors" ] ~doc)
  in
  let svg_config =
    match SvgLib.Svg.has_cairo with
    | true ->
       let doc =
         "Sets the json config rendering file for the svg generated (by the \
          $(i,realize) command) files to $(docv)."
       in
       Arg.(value & opt (some string) None & info [ "realize" ] ~docv:"FILE" ~doc)
    | false -> Term.const None in
  let debug_mode =
    let doc = "Starts $(b,acg) in debug mode: it will record and print backtraces of uncaught exceptions" in
    Arg.(value & flag & info [ "d"; "debug" ] ~doc)
  in
  let build_info = 
    let doc = "Outputs a short description of build information." in
    Arg.(value & flag & info [ "b"; "build-info" ] ~doc) in
  let man =
    [
      `S Manpage.s_description;
      `P
        "$(tname) parses each file $(i,FILE) (if any), which is supposed to be \
         a file containing ACG commands, and interpret them. If no file is provided,
         of if option $(i,interactive) is set, it then interactively run the ACG
         command interpreter.";
      `P
        "A list of the available commands is available by running the \
         \"help\" command in the interpreter.";
      `S Manpage.s_bugs;
      `P
        "Report bugs by submitting issues at \
         https://gitlab.inria.fr/ACG/dev/ACGtk/issues.";
      `P "Or report bugs to <sylvain.pogodalla@inria.fr>.";
    ]
  in
  let files = Arg.(value & pos_all string [] & info [] ~docv:"FILE") in
  Cmd.(
    v
      (info "acg" ~version:UtilsLib.Version.version ~doc ~man)
      Term.(const start $ term_width $ dirs $ with_magic $ svg_config $ force_interactive $ files
            $ step_by_step $ seed $ colors $ build_info $ debug_mode))

let () =
  Cmd.(exit @@ eval start_t)
