type config = {
  dirs : string list;
  rendering_config :  SvgLib.Svg.config;  (* optional file name for a svg configuration file *)
  step_by_step : bool;
  with_magic : bool;
  width : int option;
}
