%token PIPE SEMI EQUAL EOI LET INF COLON_EQUAL PLUS MINUS COMMA
%token COMPL_FUN COMPL_LET COMPL_ARG_VAL COMPL_ARG_NAME
%token <string> DATA ID
%token <int> INT

%start <(Environment.env -> (Value.value option * Environment.env)) option> script_command
%start <Environment.env -> (Value.value option * Environment.env)> interactive_command
%type <Environment.env -> (Value.value option * Environment.env)> command
%type <Environment.env -> (Value.value option * Environment.env)> line
%type <Value.value> term_literal
%type <Environment.func_type -> Environment.binding_list -> Environment.env -> (Environment.func_type * Environment.binding_list * (Environment.var list -> Environment.env -> Value.value option -> (Value.value option * Environment.env)))> entries
%type <Environment.func_type -> Environment.binding_list -> Environment.env -> (Environment.func_type * Environment.binding_list * (Environment.var list -> Environment.env -> Value.value option -> (Value.value option * Environment.env)))> entry
%type <Environment.arg_full list> args
%type <Environment.param_list> args_spec
%type <Environment.param> arg_spec
%type <Environment.arg_full> arg
%type <Environment.arg_parse> arg_val
%type <Environment.vlist_type * Environment.vlist_elem list> vlist

%%

(* The environment is always applied early to functions, this allows to partially
 * execute them, in order to check if the arguments are corrects before doing
 * any actual computations. The computations are done after, when the value is
 * given. *)

script_command:
| c = command SEMI { Some c }
| EOI { None }

interactive_command:
| c = command EOI { c }
| c = command SEMI EOI { c }

command:
| l = line { l }
| LET f = ID a = args_spec COLON_EQUAL e = entries { fun env -> None, (Environment.add_fun env f a $loc $loc(f) e) }
| COMPL_LET { raise (Environment.Completion (Environment.Compl_Custom ["let"])) }

line:
| t = term_literal PIPE el = entries {fun env -> let (_, _, elfun) = el Environment.Computation [] env in elfun [] env (Some t) }
| el = entries {fun env -> let (_, _, elfun) = el Environment.AtStart [] env in elfun [] env None }
| e = entry INF t = term_literal {fun env -> let (_, _, efun) = e Environment.Computation [] env in efun [] env (Some t) }

term_literal:
| d = DATA { Value.value_from_string d $loc(d) }

entries:
| e = entry { e }
| el = entries PIPE e = entry { fun fts blist env -> let (elft, elblist, elfun) = el fts blist env in let (eft, eblist, efun) = e elft elblist env in (eft, eblist, fun vl env v -> let (lv, env) = elfun vl env v in efun vl env lv) }

entry:
| f = ID a = args { Environment.call_fun f a $loc $loc(f) }
| COMPL_FUN { fun ft -> raise (Environment.Completion (Environment.Compl_Fun_name ft)) }

args:
| { [] }
| a = arg al = args { a :: al }

arg:
| PLUS n = ID { Environment.Arg (Some (n, $loc(n)), Environment.Bool_p (true, $loc)) }
| MINUS n = ID { Environment.Arg (Some (n, $loc(n)), Environment.Bool_p (false, $loc)) }
| a = arg_val { Environment.Arg (None, a) }
| n = ID EQUAL a = arg_val { Environment.Arg (Some (n, $loc(n)), a) }
| COMPL_ARG_NAME { Environment.Completion_name (None, ("", '=')) }
| PLUS COMPL_ARG_NAME { Environment.Completion_name (Some Environment.T_Bool, ("+", ' ')) }
| MINUS COMPL_ARG_NAME { Environment.Completion_name (Some Environment.T_Bool, ("-", ' '))  }

args_spec:
| { [] }
| a = arg_spec al = args_spec { a :: al }

arg_spec:
| n = ID { ((n, $loc(n)), None) }
| n = ID EQUAL a = arg_val { ((n, $loc(n)), Some a) }

arg_val:
| a = ID { Environment.Id_p (a, $loc(a)) }
| a = INT { Environment.Int_p (a, $loc(a)) }
| a = DATA { Environment.Data_p (a, $loc(a)) }
| a = vlist { let (lt, l) = a in match lt with Environment.IdList_lt -> Environment.VList_p (l, $loc(a)) | Environment.StringList_lt -> Environment.StringList_p (l, $loc(a)) }
| COMPL_ARG_VAL { Environment.Completion_val }

vlist:
| id = ID COMMA vl = vlist { let (lt, l) = vl in (lt, (Environment.Vl_elem (id, $loc(id))) :: l) }
| id = DATA COMMA vl = vlist { let (_, l) = vl in (Environment.StringList_lt, (Environment.Vl_elem (id, $loc(id))) :: l) }
| id1 = ID COMMA id2 = ID { (Environment.IdList_lt, [ Environment.Vl_elem (id1, $loc(id1)) ; Environment.Vl_elem (id2, $loc(id2)) ]) }
| id1 = DATA COMMA id2 = ID { (Environment.StringList_lt, [ Environment.Vl_elem (id1, $loc(id1)) ; Environment.Vl_elem (id2, $loc(id2)) ]) }
| id1 = ID COMMA id2 = DATA { (Environment.StringList_lt, [ Environment.Vl_elem (id1, $loc(id1)) ; Environment.Vl_elem (id2, $loc(id2)) ]) }
| id1 = DATA COMMA id2 = DATA { (Environment.StringList_lt, [ Environment.Vl_elem (id1, $loc(id1)) ; Environment.Vl_elem (id2, $loc(id2)) ]) }
| id = ID COMMA COMPL_ARG_VAL { (Environment.IdList_lt, [ Environment.Vl_elem (id, $loc(id)) ; Environment.Completion_list_val ]) }
| id = DATA COMMA COMPL_ARG_VAL { (Environment.IdList_lt, [ Environment.Vl_elem (id, $loc(id)) ; Environment.Completion_list_val ]) }
