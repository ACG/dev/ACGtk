open Logic.Lambda
open UtilsLib
open Containers
module AcgSig = AcgData.Signature.Data_Signature

(* StringTerm is used of literal terms, which are not yet parsed because
 * we need to know the signature
 * RealTerm is used for already parsed terms, when the signature has been
 * given to a function *)
type vterm =
  | StringTerm of string * Error.pos
  | RealTerm of AcgSig.t * Lambda.term * Lambda.stype * (SharedForest.SharedForest.weight option) * (string option)

type value = vterm LazyList.t

val get_term_value_in_sig : AcgSig.t -> vterm -> Lambda.term * Lambda.stype
val value_from_string : string -> Error.pos -> value

val print : bool -> value -> unit
