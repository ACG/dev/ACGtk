open Logic.Lambda
open UtilsLib
module AcgEnv = AcgData.Environment.Environment
module AcgSig = AcgData.Signature.Data_Signature
module AcgLex = AcgData.Acg_lexicon.Data_Lexicon

type sig_ref =
  | Real_sig of AcgSig.t
  | Arg_sig of int

type var =
  | Bool of bool * Error.pos
  | Int of int * Error.pos
  | Sig of AcgSig.t * Error.pos
  | Lex of AcgLex.t * Error.pos
  | Entry of AcgEnv.entry * Error.pos
  | LexList of AcgLex.t list * Error.pos
  | String of string * Error.pos
  | StringList of string list * Error.pos
  | Type of Lambda.stype * Error.pos

type completion_info =
  | Ci_Path
  | Ci_Fun
  | Ci_List of string list
  | Ci_None

type var_type =
  | T_Bool
  | T_Int
  | T_Sig
  | T_Lex
  | T_Entry
  | T_LexList
  | T_String of completion_info
  | T_StringList of completion_info
  | T_Type of sig_ref

type var_arg =
  | Bool_a of bool * Error.pos
  | Int_a of int * Error.pos
  | Sig_a of AcgSig.t * Error.pos
  | Lex_a of AcgLex.t * Error.pos
  | Entry_a of AcgEnv.entry * Error.pos
  | LexList_a of AcgLex.t list * Error.pos
  | String_a of string * Error.pos
  | StringList_a of string list * Error.pos
  | Type_a of Lambda.stype * Error.pos
  | Argument_a of int * var_type * Error.pos

(* This is a type used only during argument parsing, to be able to store types
   as strings, before parsing it to a real type in the corresponding signature. *)
type var_temp =
  | Bool_t of bool * Error.pos
  | Int_t of int * Error.pos
  | Sig_t of AcgSig.t * Error.pos
  | Lex_t of AcgLex.t * Error.pos
  | Entry_t of AcgEnv.entry * Error.pos
  | LexList_t of AcgLex.t list * Error.pos
  | String_t of string * Error.pos
  | StringList_t of string list * Error.pos
  | Type_t_s of (sig_ref * string) * Error.pos
  | Type_t_r of Lambda.stype * Error.pos
  | Argument_t of int * var_type * Error.pos

(* This type is for storing the type of function parameters. They may contain
   a default value. *)
type var_spec =
  | Bool_s of bool option
  | Int_s of int option
  | Sig_s of AcgSig.t option
  | Lex_s of AcgLex.t option
  | Entry_s of AcgEnv.entry option
  | LexList_s of (AcgLex.t list) option
  | String_s of string option * completion_info
  | StringList_s of (string list) option * completion_info
  | Type_s of sig_ref * Lambda.stype option

type vlist_type =
  | StringList_lt
  | IdList_lt

type vlist_elem =
  | Vl_elem of string * Error.pos
  | Completion_list_val

type arg_parse =
  | Int_p of int * Error.pos
  | Id_p of string * Error.pos
  | Data_p of string * Error.pos
  | Bool_p of bool * Error.pos
  | VList_p of vlist_elem list * Error.pos
  | StringList_p of vlist_elem list * Error.pos
  | Completion_val

type var_type_parse =
  | T_Bool_p of Error.pos
  | T_Int_p of Error.pos
  | T_Sig_p of Error.pos
  | T_Lex_p of Error.pos
  | T_Entry_p of Error.pos
  | T_VListF_p of Error.pos
  | T_LexList_p of Error.pos
  | T_LexListF_p of Error.pos
  | T_StringList_p of Error.pos * completion_info
  | T_StringListF_p of Error.pos * completion_info option
  | T_String_p of Error.pos * completion_info
  | T_Type_p of sig_ref option * Error.pos
  | T_Id_p of Error.pos
  | T_Data_p of Error.pos

type arg_full =
  | Arg of (string * Error.pos) option * arg_parse
  | Completion_name of var_type option * (string * char)


type binding_list = ((string * Error.pos) * var_type_parse option * bool) list
type param = (string * Error.pos) * arg_parse option
type param_list = param list

type func_type =
  | All
  | AtStart
  | Generation
  | Computation
  | Consumption
  | Special
  | End

type func =
  | Generation_f of (var list -> env -> Value.value)
  | Computation_f of (var list -> env -> Value.value -> Value.value)
  | Consumption_f of (var list -> env -> Value.value -> unit)
  | Special_f of (var list -> env -> env)

and func_spec = {
  name : string;
  help_text : string;
  args : (string * var_spec) list;
  f : func
}

and env = {
  config : Config.config;
  acg_env : AcgEnv.t;
  functions : func_spec list;
  last_value : Value.value option
}

type completion =
  | Compl_Fun_name of func_type
  | Compl_Arg_name of string * (string * char) * var_type option * string list
  | Compl_Arg_val of var_spec * string list
  | Compl_None
  | Compl_Custom of string list

exception Completion of completion

val add_fun : env -> string -> param_list -> Error.pos -> Error.pos ->
  (func_type -> binding_list -> env -> (func_type * binding_list * (var list -> env -> Value.value option -> Value.value option * env)))
  -> env
val call_fun :
  string -> arg_full list -> Error.pos -> Error.pos -> func_type -> binding_list -> env ->
    (func_type * binding_list * (var list -> env -> Value.value option -> Value.value option * env))

(** This function is used to generate the documentation in odoc format.
    It uses the function specifications and the help texts defined in the module [Functions]. *)
val doc_pp : Format.formatter -> func_spec list -> unit

val print_help : env -> string -> unit
val short_print : env -> unit

type completion_element =
  | Ce_String of string * string * char
  | Ce_Path

val gen_completions : completion -> string -> env -> completion_element list
