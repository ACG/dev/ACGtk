module Lexing_o = Lexing

(** This module contains the type for syntax errors in scripts. *)
module Syntax_l : sig
  type t =
    | UnexpectedEOI (** [UnexpectedEOI] is for valid but incomplete inputs. *)
    | TrailingChars (** [TrailingChars] is for invalid inputs containing a valid prefix. *)
    | SyntaxError of int (** [SyntaxError i] is for all other syntax errors, with
        [i] the number given by Menhir to select the correct message. *)
    | Other (** [Other] is for unknown syntax errors. *)

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module SyntaxErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Syntax_l.t -> 'a
end

(** This module contains the type for lexing errors in scripts. *)
module Lexing_l : sig
  type t =
    | Unclosed of string (** [Unclosed c] is for opened but unclosed delimiter
        char [c] (for example, quotes). *)
    | UnterminatedComment (** [UnterminatedComment] is a special case of
        [Unclosed] for comments. *)
    | UnstartedComment (** [UnstartedComment] is raised when the comment closing
        token appear before the opening one. *)
    | BadChar of string (** [BadChar c] is raised when a char [c] can't appear in
        any lexical tokens. *)
    | Malformed (** [Malformed] if raised when an invalid UTF-8 sequence is
        encountered in the input. *)

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module LexingErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Lexing_l.t -> 'a
end

(** This module contains the type for typing errors, in the scripting language,
    not in the lambda-terms. *)
module Type_l : sig
  type t =
    | Variable of string * string * string * string * string (**
        [Variable (func, param, var, exp, act)] represents a typing error of the
        variable [var], given as parameter [param] of function [func]. [exp] is
        the expected type, and [act] is the actual type of [var]. *)
    | Literal of string * string * string * string (**
        [Literal (func, param, ext, act)] represents a typing error of a
        literal value, given as paramater [param] of function [func]. [exp] is
        the expected type, and [act] is the actual type of the literal value. *)
    | DefaultValue of string * string * string * string (**
        [DefaultValue (func, param, ext, act)] represents a typing error of the
        default value of parameter [param] of new function [func]. [exp] is the
        expected type, and [act] is the actual type. *)
    | Other (* [Other] is for an unknown error. *)

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module TypeErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Type_l.t -> 'a
end

(** This module contains the type for other script errors. *)
module Script_l : sig
  type t =
    | NoSignatureType of string (** [NoSignatureType ty] is raised when a type
        [ty] is used without signature to interpret it. *)
    | NoSignatureTerm (* [NoSignatureTerm] is raised when the result of a
        command is an unparsed term. *)
    | WrongSignature of string * string (* [WrongSignature (sig1, sig2)] is
        raised when a term of a signature [sig1] is used with an other
        signature [sig2]. *)
    | OnlyLexicon of string (* [OnlyLexicon sig] is raised when a signature
        [sig] is given to a function which only accepts lexicons. *)
    | ArgumentMissing of string * string (* [ArgumentMissing (param, func)] is
        raised when the value of a non-optional parameter [param] of function
        [func] is missing. *)
    | DifferentSig of
        string
        * (AcgData.Signature.Data_Signature.t
          * AcgData.Signature.Data_Signature.t) option (**
        [DifferentSig (var, Some (sig1, sig2))] is raised when a type variable
        is used in at least two contexts with different signatures [sig1] and
        [sig2]. *)
    | AlreadyExistingFun of string (** [AlreadyExistingFun func] when a function
        [func] is defined with a name already used. *)
    | UnusedVariable of string (** [UnusedVariable var] is raised when a
        function is defined with a unused variable [var] as paramenter. *)
    | UnknownFunction of string (** [UnknownFunction func] is raised when an
        unknown function [func] is called. *)
    | UnknownParameter of string * string (** [UnknownParameter (func, param)]
        when a function [func] is called with an unknown parameter [param]. *)
    | ParameterRepeated of string * string (** [ParameterRepeated (func, param)]
        is raised when a function [func] is called with at least twice the same
        parameter [param]. *)
    | TooMuchArgs of string (** [TooMuchArgs func] is raised when a function is
        called with too much unnamed arguments. *)
    | NoLastValue (** [NoLastValue] is raised when the function [last] is called
        before executing a command returning a value. *)
    | InvalidSpecialFuncCall of string (** [InvalidSpecialFuncCall func] is raised
        when a special function [func] is used in a command with multiple functions. *)
    | MissingFuncTermList of string (** [MissingFuncTermList func] is raised when
        a function [func] which needs terms as input is called without terms. *)
    | TermListNotAllowed of string (** [TermListNotAllowed func] is raised when
        a function [func] which does not take terms as input is called with terms. *)
    | EmptyPipe of string (** [EmptyPipe func] is raised when a function [func] is
       called with an invalid pipe. *)
    | EmptyHelp of string (** [EmptyHelp func_preffix] is raised when the function
        "help" is called with an argument [func_preffix] that matches no functions
        of the environment *)
    | AbsSigMismatch of AcgData.Acg_lexicon.Data_Lexicon.(t * t)
    (** [lexicon (lex1,lex)] occurs when the abstract signature of the
        lexicon [lex2] does not match the current abstract signature
        of [lex1] in a [realize] command. *)
    | Other (** [Other] is for an unknown error. *)

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module ScriptErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Script_l.t -> 'a
end
