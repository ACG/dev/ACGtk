type result =
  | Continue of Environment.env
  | Continue_Error
  | Stop

val interactive : Environment.env -> result
val script : Sedlexing.lexbuf -> Environment.env -> Environment.env
