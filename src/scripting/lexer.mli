type lex_token =
  | PartialToken of string * Errors.Lexing_l.t
  | Token of Parser.token * string

val lex : Sedlexing.lexbuf -> lex_token
