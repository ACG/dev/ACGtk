open DatalogLib.Datalog_AbstractSyntax
module Store = DatalogLib.UnionFind.StoreAsMap
(*struct
    include PersistentArray.PersistentArray
    let empty i =
      let value,_ = ConstGen.get_fresh_id (ConstGen.init ()) in
      init i (fun _ -> value)
  end
*)

module Datalog = DatalogLib.Datalog.Make (Store)

let main query edb filename =
  match DatalogLib.Dl_parse_functions.parse_program filename with
  | None -> ()
  | Some p -> (
      let prog = p AbstractSyntax.Proto_Program.empty in
      Logs.debug (fun m ->
          m "Current symbol tables:@,@[<v>  @[%a@]@]"
            AbstractSyntax.Predicate.PredIdTable.pp
            prog.AbstractSyntax.Proto_Program.pred_table);
      let sep = String.make 15 '*' in
      Logs.app (fun m -> m "%s%!" sep);
      let abs_program = AbstractSyntax.Program.make_program prog in
      Logs.app (fun m ->
          m "Create the abstract program and print it...@,  @[<v>%a@]"
            (AbstractSyntax.Program.pp ~with_position:false ~with_id:false) abs_program);
      Logs.app (fun m -> m "Done.");
      Logs.app (fun m -> m "%s" sep);
      let program = Datalog.Program.make_program abs_program in
      Logs.app (fun m ->
          m "Create the internal program and print it...@,  @[<v>%a@]"
            (AbstractSyntax.Program.pp ~with_position:false ~with_id:false)
            (Datalog.Program.to_abstract program));
      Logs.app (fun m -> m "Done.");
      Logs.app (fun m -> m "%s" sep);
      let program =
        match edb with
        | None ->
            let () =
              Logs.debug (fun m -> m "I didn't find an edb file to parse.")
            in
            program
        | Some edb_filename -> (
            let () = Logs.debug (fun m -> m "I found an edb file to parse.") in
            let to_be_added =
              DatalogLib.Dl_parse_functions.parse_edb edb_filename
            in
            match to_be_added with
            | None -> program
            | Some add ->
                Datalog.Program.add_e_facts program
                  (add
                     Datalog.Program.
                       ( program.pred_table,
                         program.const_table,
                         program.rule_id_gen )))
      in
      let derived_facts, derivations = Datalog.Program.seminaive program in
      Logs.app (fun m ->
          m "I could derive the following facts:@,@[<v>  @,%a@,@]"
            (Datalog.Predicate.pp_facts program.Datalog.Program.pred_table
               program.Datalog.Program.const_table)
            derived_facts);
      Logs.app (fun m ->
          m "With the following derivations:@,@[<v>  @,%a@]"
            (Datalog.Predicate.pp_facts_from_premises
               program.Datalog.Program.pred_table
               program.Datalog.Program.const_table)
            derivations);
      match query with
      | None ->
          Datalog.Predicate.format_derivations2
            program.Datalog.Program.pred_table
            program.Datalog.Program.const_table derivations
      | Some s -> (
          let as_q = DatalogLib.Dl_parse_functions.parse_query s in
          match as_q with
          | None -> ()
          | Some pred ->
              let () =
                Logs.app (fun m ->
                    m
                      "Facts (and their derivations) matching the query \"%s\":\n"
                      s)
              in
              let q, _t1, _t2 =
                pred Datalog.Program.(program.pred_table, program.const_table)
              in
              Datalog.Predicate.format_derivations2 ~query:q
                program.Datalog.Program.pred_table
                program.Datalog.Program.const_table derivations))

let usage_msg =
  Format.sprintf
    "Usage: @[<v>db_test [-edb edb_file] [-q query] file@,db_test --help@]"

let edb_file = ref None
let query = ref None

let options =
  [
    ( "-edb",
      Arg.String (fun s -> edb_file := Some s),
      "Add the specified file as an edb (it should include only extensional \
       facts)." );
    ( "-q",
      Arg.String (fun s -> query := Some s),
      "Only outputs the derivations satisfying the specified query. The latter \
       is of the form \"SYM(params)?\" where params is a n-ary tuple of \
       constants (integers) or variables (identifiers), and n is the arity of \
       SYM." );
  ]

let () =
  let () = UtilsLib.Xlog.set_level ~app:"db_test" Logs.Warning in
  (*  let () = UtilsLib.Log.set_level ~app:"db_test" Logs.Debug in *)
  if Array.length Sys.argv > 1 then
    (* Arg.parse options (fun s -> parse_file !query !edb_file s) usage_msg *)
    Arg.parse options (fun s -> main !query !edb_file s) usage_msg
  else
    let () =
      Printf.printf "You should at least specify a database to read\n%!"
    in
    Printf.printf "%s\n!" usage_msg
