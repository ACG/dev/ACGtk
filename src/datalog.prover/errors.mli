module Syntax_l : sig
  type t =
    | BadArity of (string * int * int)
    | UnknownPredicate of string
    | ParseError of string

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module SyntaxErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Syntax_l.t -> 'a
end

module Lexing_l : sig
  type t =
    | UnterminatedComment
    | UnstartedComment
    | BadChar of string
    | Malformed

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module LexingErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Lexing_l.t -> 'a
end
