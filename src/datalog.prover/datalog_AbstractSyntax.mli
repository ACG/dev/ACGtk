open UtilsLib
open IdGenerator

module Log : Xlog.MyLOG
(** [Log] is the log module for Datalog_AbstractSyntax *)

module VarGen : IdGen_TYPE
module ConstGen : IdGen_TYPE
module RuleIdMap : Map.S with type key = int

(** These modules are the abstract syntactic representations of
    predicates and rules *)

module AbstractSyntax : sig
  module Predicate : sig
    type term = Var of VarGen.id | Const of ConstGen.id

    module TermSet : Set.S with type elt = term

    val pp_terms : ConstGen.Table.table -> Format.formatter -> TermSet.t -> unit

    type pred_id

    val pp_pred_id : Format.formatter -> pred_id -> unit
    
    module PredIdMap : Map.S with type key = pred_id
    module PredIdTable : CorrespondanceTableTYPE with type identifier = pred_id
    module PredIds : Set.S with type elt = pred_id

    type predicate = {
      p_id : pred_id;
      arity : int;
      arguments : term list;
          (** It is assumed that the size of the list is the
                       arity *)
    }

    val pp :
      ?position:int ->
      ?with_id:bool ->
      ?with_arity:bool ->
      PredIdTable.table ->
      ConstGen.Table.table ->
      Format.formatter ->
      predicate ->
      unit

    val compare : ?with_arguments:bool -> predicate -> predicate -> int
    (** [compare p1 p2] returns a positive integer if [p1] is greater
       than [p2]. If [with_arguments] is set to true (default), then
       the comparison relies on the actual value of the arguments as
       well. Otherwise, it does not take then into account. *)

    val fake_pred_id : pred_id

    val get_variables : predicate -> TermSet.t
    (**[get_variables pred] returns the set of all terms that are
       variables in the argument list of the predicate [pred] *)

    val get_variables_of_preds : predicate list -> TermSet.t
    (**[get_variables_of_preds preds] returns the set of all terms
       that are variables in the argument list of all the predicates
       in [preds] *)

    (*    val string_of_predicate_name : predicate -> PredIdTable.table -> string *)
    (** [string_of_predicate_name pred pred_id_table] Returns the name of the predicate. *)

    val copy_predicate : new_id:pred_id -> predicate -> predicate
    (**[copy_predicate ~new_id:id pred] copies the predicate [pred]
       (arity and content) to a new predicate with id [id] *)
  end

  module Proto_Rule : sig
    type t = {
      proto_id : int;
      proto_lhs : Predicate.predicate;
      proto_rhs : Predicate.predicate list;
          (** represents the predicates of the rule.*)
    }

    val pp :
      Predicate.PredIdTable.table ->
      ConstGen.Table.table ->
      Format.formatter ->
      t ->
      unit
  end

  module Rule : sig
    type rule = {
      id : int;
      lhs : Predicate.predicate;
      e_rhs : (Predicate.predicate * int) list;
          (** represents the extensionnal predicates of the
		   rule. The [int] represents the position in the rule *)
      i_rhs : (Predicate.predicate * int) list;
          (** represents the intensionnal predicates of the rule.
		  The [int] represents the position in the rule *)
      i_rhs_num : int;
          (** stores the number of intensional predicates occurring in the
		 rule *)
      rhs_num : int;
          (** stores the number of predicates occurring in the
		 rule body *)
    }

    val pp :
      ?with_position:bool ->
      ?with_id:bool ->
      ?with_arity:bool ->
      Predicate.PredIdTable.table ->
      ConstGen.Table.table ->
      Format.formatter ->
      rule ->
      unit

    val proto_rule_to_rule : Proto_Rule.t -> Predicate.PredIds.t -> rule

    module Rules : Set.S with type elt = rule
    module RuleMap : Map.S with type key = rule

    val extend_head_id_map_to_rules :
      Predicate.pred_id ->
      rule ->
      Rules.t Predicate.PredIdMap.t ->
      Rules.t Predicate.PredIdMap.t

    val set_new_id : int -> rule -> rule
    (** [set_new_id id rule] returns a rule similar to [rule] except
       that its id is [id] *)

    val set_new_id_from_gen : rule -> IntIdGen.t -> rule * IntIdGen.t
    (** [set_new_id_from_gen] returns pair [(rule',from_gen')] wherer
       [rule'] is a rule similar to [rule] excepts that it has a new
       id generated from [from_gen] generator, and the latter has been
       updated to [from_gen']. *)

    val get_variables_in_rule : rule -> Predicate.TermSet.t
    (**[get_variables_in_rule rule] returns the set of variables in the rule [rule] *)

    val get_subgoal : rule -> int -> Predicate.predicate * int
    (** [get_subgoal rule i] returns the predicate at the position [i]
       in the body of the rule [rule] *)
  end

  module Proto_Program : sig
    type t = {
      rules : Proto_Rule.t list;
      pred_table : Predicate.PredIdTable.table;
      const_table : ConstGen.Table.table;
      i_preds : Predicate.PredIds.t;
      rule_id_gen : IntIdGen.t;
      pred_to_rules : Utils.IntSet.t Predicate.PredIdMap.t;
    }

    type tables =
      Predicate.PredIdTable.table * (VarGen.Table.table * ConstGen.Table.table)

    val empty : t

    val extension :
      Predicate.PredIdTable.table -> ConstGen.Table.table -> IntIdGen.t -> t
    (** [extension pred_table const_table id_gen] returns an almost
       empty proto program. This almost empty proto program is meant
       to serve as extension of an actual program *)

    val add_proto_rule :
      (tables -> Predicate.predicate * tables)
      * (tables -> Predicate.predicate list * tables) ->
      t ->
      t
  end

  module Program : sig
    type program = {
      rules : Rule.Rules.t;
      pred_table : Predicate.PredIdTable.table;
      const_table : ConstGen.Table.table;
      i_preds : Predicate.PredIds.t;
      rule_id_gen : IntIdGen.t;
      head_to_rules : Rule.Rules.t Predicate.PredIdMap.t;
      e_pred_to_rules : Rule.Rules.t Predicate.PredIdMap.t;
    }

    type modifier = {
      modified_rules : Rule.Rules.t;
      new_pred_table : Predicate.PredIdTable.table;
      new_const_table : ConstGen.Table.table;
      new_i_preds : Predicate.PredIds.t;
      new_e_preds : Predicate.PredIds.t;
      new_rule_id_gen : IntIdGen.t;
    }

    val make_program : Proto_Program.t -> program
    val extend : program -> Proto_Program.t -> program

    val is_in_idb : Predicate.predicate -> program -> bool
    (** [is_in_idb pred prog] returns [true] if the predicate [pred] is an
       intensional predicate for the program [prog] *)

    val is_head : Predicate.predicate -> Rule.rule -> bool
    (** [is_head pred rule] returns true if the predicate [pred]
       matches with the head of the rule [rule] *)

    val match_rules : Predicate.predicate -> program -> Rule.Rules.t
    (** [match_rules pred prog] returns the set of rules of program
       [prog] whose heads match with [pred] *)

    val get_rule_by_id : program -> int -> Rule.Rules.elt
    (** [get_rule_by_id program id] Get the rule matching the id *)
    
    val pp : 
           ?with_position:bool ->
           ?with_id:bool -> Format.formatter -> program -> unit
  end
end
