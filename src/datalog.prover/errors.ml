open UtilsLib.Error

module Syntax_l =
  struct
    type t =
      | BadArity of (string * int * int)
      | UnknownPredicate of string
      | ParseError of string

    let kind = "Datalog syntax"

    let pp fmt err =
      match err with
      | BadArity (sym, expected_a, written_a) ->
        Format.fprintf fmt
          "Predicate@ symbol@ \"%s\"@ is@ defined@ with@ arity@ %d@ \
           while@ used@ with@ arity@ %d"
          sym expected_a written_a
      | UnknownPredicate pred ->
        Format.fprintf fmt "The predicate \"%s\" is not a predicate of the program" pred
      | ParseError s -> Format.fprintf fmt "%a" UtilsLib.Error.pp_text s
  end

module SyntaxErrors = ErrorManager(Syntax_l)

module Lexing_l =
  struct
    type t =
      | UnterminatedComment
      | UnstartedComment
      | BadChar of string
      | Malformed

    let kind = "ACG lexing"

    let pp fmt err =
      match err with
      | UnterminatedComment -> Format.fprintf fmt "Unclosed@ comment"
      | UnstartedComment -> Format.fprintf fmt "No@ comment@ opened@ before@ this@ closing@ of@ comment"
      | BadChar tok -> Format.fprintf fmt "Bad@ char:@ \"%s\"" tok
      | Malformed -> Format.fprintf fmt "Malformed@ UTF-8@ input"
  end

module LexingErrors = ErrorManager(Lexing_l)
