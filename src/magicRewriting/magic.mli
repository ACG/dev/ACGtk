open DatalogLib.Datalog_AbstractSyntax
module ASPred = AbstractSyntax.Predicate
module ASProg = AbstractSyntax.Program
open DatalogLib.Datalog

module Log : UtilsLib.Xlog.MyLOG
(** [Log] is the log module for NewMagic *)


type extra_pred_type =
  | Magic
  | Supp_zero
  | Supp_k


val make_magic :
  AbstractSyntax.Program.program ->
  DatalogLib.Datalog_AbstractSyntax.AbstractSyntax.Program.program
  * extra_pred_type ASPred.PredIdMap.t
  * int RuleIdMap.t
(** [make magic unique_binding_prog] returns a 4-uple [(magic_prog,
   magic_preds, magic_to_unique_binding_rule_map,
   magic_to_original_pred_map)] where:

    - [magic_prog] is the resulting magic program. IMPORTANT:
   predicates ids/name relations for existing predicate in
   [unique_binding_prog] is unchanged

    - [magic_preds] a record containing the different magic and
   supplementary predicates that have been added

    - [magic_to_unique_binding_rule_map] is a map from magic rules to
   the corresponding rules of the unique binding program

    - [magic_to_original_pred_map] is a map from ids of predicates of
   the magic program to the corresponding predicate ids of the
   original program

    *)

val query_to_seed : ASPred.predicate -> ASProg.program -> ASProg.program
(** [query_to_seed query program] Build the seed from the query and
    update the program consequently.*)

val query_to_seed_concrete :
  ASPred.predicate -> Datalog.Program.program -> Datalog.Program.program
(** [query_to_seed query program] Build the seed from the query and
   update the program consequently.*)
