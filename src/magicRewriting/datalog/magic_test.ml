open DatalogLib.Datalog_AbstractSyntax
open MagicRewriting

let parse_file filename =
  match DatalogLib.Dl_parse_functions.parse_program filename with
  | None -> ()
  | Some p ->
    let prog = p AbstractSyntax.Proto_Program.empty in
    Logs.debug (fun m -> m "Correspondance table for the proto program is:@,@[<v>  @[%a@]@]"
                   AbstractSyntax.Predicate.PredIdTable.pp
                   prog.AbstractSyntax.Proto_Program.pred_table);
      (*  let sep = String.make 15 '*' in *)
      let abs_program = AbstractSyntax.Program.make_program prog in
      let abs_programs = Rewriting.rewrite_programs abs_program in
      Rewriting.QueryMap.iter
        (fun (p_id, bfs) (prog, _ctx) ->
          Format.printf "Unique binding Program for query \"%s_%s\":@,@[<v>  @[%a@]@]@,"
            (AbstractSyntax.Predicate.PredIdTable.find_sym_from_id p_id
               prog.AbstractSyntax.Program.pred_table)
            (Adornment.to_string bfs)
            (AbstractSyntax.Program.pp ~with_position:false ~with_id:false)
            prog)
        abs_programs

let usage_msg = "Usage: magic_test file"
let options = []

let () =
  let () = Printexc.record_backtrace true in
  let () = UtilsLib.Xlog.set_level ~app:"acgc" ~colored:false Logs.Warning in
  let () =
    Logs.Src.set_level MagicRewriting.Rewriting.Log.src (Some Logs.Debug)
  in
  let () =
    Logs.Src.set_level MagicRewriting.Unique_binding.Log.src (Some Logs.Debug)
  in
  let () = Logs.Src.set_level MagicRewriting.Rgg.Log.src (Some Logs.Debug) in
  Arg.parse options (fun s -> parse_file s) usage_msg
