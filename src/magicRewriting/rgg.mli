(**

Module to handle the Rule/Goal Graph construction.


12.8 Rule/Goal Graph see p 795-799 of Principles of Database and
Knowledge-Base Systems II, Ullman
*)

open DatalogLib
module ASProg = Datalog_AbstractSyntax.AbstractSyntax.Program
module ASPred = Datalog_AbstractSyntax.AbstractSyntax.Predicate
module ASRule = Datalog_AbstractSyntax.AbstractSyntax.Rule

module Log : UtilsLib.Xlog.MyLOG
(** [Log] is the log module for Rgg *)

type rule_node = {
  rule : ASRule.rule;
  adorned_head : Adornment.status list;
  position : int;
  bound_vars : ASPred.TermSet.t;
  free_vars : ASPred.TermSet.t;
}

type vertex =
  | Goal of (ASPred.predicate * Adornment.status list)
  | Rule of rule_node  (**A node is either a Goal or a Rule *)

module Rg_graph : Graph.Sig.P with type V.t = vertex

val graph_to_dot : Rg_graph.t -> ASProg.program -> string -> unit
(**[graph_to_dot rgg program filename] Build a dot file from a rgg graph *)

val build_rgg :
  ASProg.program ->
  ASPred.predicate * Adornment.status list ->
  Rg_graph.t
(**[build_rgg program query] Build the rule/goal graph for [program]
   and [query]. *)
