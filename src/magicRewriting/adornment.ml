module ASPred = DatalogLib.Datalog_AbstractSyntax.AbstractSyntax.Predicate

type status =
  | Bound
  | Free

type t = status list

let adorn ~bound_variables = function 
  | ASPred.Const _ -> Bound
  | var when ASPred.TermSet.mem var bound_variables -> Bound
  | _ -> Free

let adornment ~bound_variables pred =
  let rev_bindings, bound_variables =
    List.fold_left
      (fun (l_rev_bindings, ctx) term ->
         (adorn ~bound_variables term)::l_rev_bindings, ASPred.TermSet.add term ctx)
      ([], bound_variables)
      pred.ASPred.arguments in
  List.rev rev_bindings, bound_variables
  

let status_to_string = function
  | Bound -> "b"
  | Free -> "f"

let compare_status b1 b2 =
  match b1, b2 with
  | Bound, Bound
  | Free, Free -> 0
  | Bound, Free -> -1
  | Free, Bound -> 1


let compare l1 l2 = List.compare compare_status l1 l2

let to_string l =
  UtilsLib.Utils.string_of_list "" status_to_string l

let adorned_predicate_to_string ~pred_table (pred, bindings) =
  let pred_name = ASPred.(PredIdTable.find_sym_from_id pred.p_id pred_table) in
  let adornment = to_string bindings in
  Printf.sprintf "%s_%s" pred_name adornment
