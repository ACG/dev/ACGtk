(** This module implements free/bound adornments *)

module ASPred = DatalogLib.Datalog_AbstractSyntax.AbstractSyntax.Predicate

type status =
  | Bound
  | Free
  (** the status that can be given to a variable (or a constant) *)

val status_to_string : status -> string
(** [status_to_string st] returns a string describing the status
   (basically either ["f"] or ["b"]). *) 

type t = status list
(** The type of adornments *)

val adornment :
  bound_variables:ASPred.TermSet.t ->
  ASPred.predicate ->
  t * ASPred.TermSet.t
(** [adornment ~bound_variables pred] returns a pair [(bindings,
   new_bound_variables)] where [bindings] is the status of each of the
   parameters of the predicate [pred] according to the
   [bound_variables] set, and new_bound_variables is the
   [bound_variables] augmented with the variables occuring as
   parameters of [pred]. *)

val compare : t -> t -> int
(** [compare ad1 ad2] returns 0 if the adornments are equal, -1 if
   [ad1] is smaller than [ad2], and 1 otherwise. *)

val to_string : t -> string
(** [to_string ad] returns a string (a word made of [b]'s and [f]'s)
   describing the adornment. *)

val adorned_predicate_to_string :
  pred_table:ASPred.PredIdTable.table ->
  (ASPred.predicate * t) -> string
(** [adorned_predicate_to_string ~pred_table ad] returns the string
   corresponding to the adorned predicate. *)
