(** This module the construction of a new program where every binding
pattern for a predicate p is unique, see p 800 of Principles of Database and
Knowledge-Base Systems II, Ullman *)

open DatalogLib
module ASPred = Datalog_AbstractSyntax.AbstractSyntax.Predicate
module ASRule = Datalog_AbstractSyntax.AbstractSyntax.Rule
module ASProg = Datalog_AbstractSyntax.AbstractSyntax.Program

module Log : UtilsLib.Xlog.MyLOG
(** [Log] is the log module for Unique_binding *)

(*val make_unique_binding :
  ASProg.program ->
  Rgg.Rg_graph.t ->
  ASProg.program
  * ASPred.pred_id ASPred.PredIdMap.t
  * (int * int) Datalog_AbstractSyntax.RuleIdMap.t
*)

(** [make_unique_binding program rgg] Takes a program and a rule/goal graph
and returns a program where every binding
pattern for a predicate p is unique

 *)

type ad_pred_key = {
  key_rule : int;
  key_head_ad : Adornment.status list;
  key_pred : ASPred.pred_id; (* Probably useless *)
  key_pos : int;
}

module AdPredMap : Map.S with type key = ad_pred_key
module AdornmentTrie : UtilsLib.Table.TABLE with type key = Adornment.status list

type data = {
  adorned_prog : ASProg.program;
  unadorned_map : ASPred.pred_id ASPred.PredIdMap.t;
  (* map from the id of the adorned predicate to the id of
     the predicate it replaces *)
  rule_to_rule_map : (int * int) Datalog_AbstractSyntax.RuleIdMap.t;
  (* map from the id of the new rule to the pair
     consisting of the id of the rule it replaces and the
     i_rhs_num *)
  adornments : Adornment.status list AdPredMap.t;
  (* a map to keep track of the adornments of the
     predicates in a given rule with a given adornment
     (avoids parsing the predicate symbol) *)
  pred_map : ASPred.pred_id AdornmentTrie.t ASPred.PredIdMap.t;
      (* a map from the p_id of the original program to a map
         (as a trie) from adornments to the new p_ids *)
}

val derive_program : Rgg.Rg_graph.t -> ASProg.program -> data
