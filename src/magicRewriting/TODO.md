+ vérifier tous les printf
+ vérifier tous les appels à .of_list, à map, à .elements, .bindings, etc.
+ à la construction du Rgg, ajouter un map des règles vers les nœuds du RGG qui leur correspondent
+ vérifier si on peut se passer de donner un nom explicite (ou l'utiliser) pour les prédicats ornés (map des et vers les prédicats d'origine (id <-> (id,bfs) ?)
+ lors du recalcul des positions dans les règles, mettre à jour les règles dans le map tête->règles
+ essayer de ne pas faire dépendre la réécriture de la valeur de la position dans la règles (mais quand même choisir un ordre pour les nœuds Rule du RGG) -> essayer d'optimiser cet ordre. Voir heuristique d'ordonnancement dans Ullman, Jeffrey D. (1990). Principles of Database and Knowledge-Base Systems : Volume II : The New Technologies
+ rajouter concurrence sur l'algo semi-naïf (Ganguly, Sumit, Avi Silberschatz et Shalom Tsur (1992). « Parallel Bottom-up Processing of Datalog Queries ». In : J. Log. Program. 14.1-2, p. 101–126. issn : 0743-1066. doi : 10.1016/0743-1066(92) 90048-8.)
+ *vérifier le sens mapping* old_id -> new_id dans rules_id_from_preprocess (est-ce que c'est parce qu'ils ne voulaient pas avoir de map vers des sets que ce n'est pas dans l'autre sens ?)
+ remplace le champs i_pred des programmes par un map vers l'arité
