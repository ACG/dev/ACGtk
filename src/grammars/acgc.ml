(**************************************************************************)
(*                                                                        *)
(*                 ACG development toolkit                                *)
(*                                                                        *)
(*                  Copyright 2008-2024 INRIA                             *)
(*                                                                        *)
(*  More information on "https://acg.loria.fr/"                     *)
(*  License: CeCILL, see the LICENSE file or "http://www.cecill.info"     *)
(*  Authors: see the AUTHORS file                                         *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(*                                                                        *)
(**************************************************************************)

open Cmdliner
open UtilsLib
module Env = AcgData.Environment.Environment
module Sg = AcgData.Signature.Data_Signature
module Lexicon = AcgData.Acg_lexicon.Data_Lexicon
module Actual_parser = Grammars.Parsers

(** This module correspond to the main funtion of the ACG compiler and
its command line interface. *)

type color_mode =
  | C_auto
  | C_yes
  | C_no

let parse ~with_magic filename dirs (name, env) =
  let () =
    Logs.debug (fun m ->
      m "The environment currently has %d signature(s) and %d lexicon(s)."
        (Env.sig_number env) (Env.lex_number env))
  in
    let (ft, new_env) = Dump.load_env ~with_magic filename dirs env Error.dummy_pos in
    match ft with
    | Dump.Data ->
      let new_name = (Filename.remove_extension filename) ^ ".acgo" in
      (Some new_name, new_env)
    | Dump.Object -> (name, new_env)


let parse_files ~with_magic status dirs files =
  List.fold_left (fun acc file -> parse ~with_magic file dirs acc) status files

let output_env ?output_file ~force name env =
  let actual_output_file =
    match output_file with None -> (Filename.basename name) | Some f -> f
  in
  let () = Dump.save_env ~force actual_output_file env in
  Logs.app (fun m -> m "Output written on: \"%s\"" actual_output_file)

let main_function term_width with_magic output_file dirs files color_mode verbosity debug_mode =
  let () = if debug_mode then Printexc.record_backtrace true in
  let () = UtilsLib.Utils.resize_terminal ~width:term_width in
  let colors =
    match color_mode with
    | C_auto -> !ANSITerminal.isatty Unix.stdout
    | C_yes -> true
    | C_no -> false in
  let () = UtilsLib.Xlog.set_level ~app:"acgc" ~colored:colors Logs.Warning in
  let timer_level =
    match verbosity with
    | 0 -> Logs.Info
    | 1 -> Logs.Debug
    | _ ->  Logs.Debug in
  let () = Timer.set_level timer_level in
  let () = Grammars.AcgcLog.set_acgc_log () in
  let dirs = match dirs with [ "" ] -> dirs | _ -> "" :: dirs in
  try
    let result = parse_files ~with_magic (None, Env.empty) dirs files in
    match result, output_file with
    | (None, _), None ->
      let () =
        Printf.fprintf stderr
          "No ouput file is produced\nPlease specify an output file.\n%!"
      in
      Cmd.Exit.ok
    | (None, env), Some n ->
      let () = output_env ~force:true n env in
      Cmd.Exit.ok
    | (Some n, env), _ ->
      let () =
        Logs.debug (fun m ->
            m
              "The environment currently has %d signature(s) and %d \
               lexicon(s)."
              (Env.sig_number env) (Env.lex_number env))
      in
      let () = output_env ?output_file ~force:false n env in
      Cmd.Exit.ok
  with
  | e -> Error.print_error e None; Cmd.Exit.some_error

let main =
  let doc = "Compile ACG files into a binary representation." in
  let output_file =
    let doc =
      "Sets $(docv) as the output file, instead of the default which \
       is derived from the last given source file. If no source file \
       is provided (only object files are provided), then all the \
       signatures and lexicons will be available in $(docv)."
    in
    Arg.(
      value & opt (some string) None & info [ "o"; "output" ] ~docv:"FILE" ~doc)
  in
  let dirs =
    let doc =
      "Sets $(docv) as a directory in which file arguments can be looked for."
    in
    Arg.(value & opt_all dir [ "" ] & info [ "I"; "include" ] ~docv:"DIR" ~doc)
  in
  let with_magic =
    let doc =
      "Toggles on generating magic programs (experimental \
       feature). Parsing with magic will be available in $(b,acg). Be \
       aware that using this option may cause generated object files \
       to be very large."
    in
    Arg.(value & flag & info [ "m"; "magic" ] ~doc) in
  let colors =
    let doc = "Controls the colors in output. Use $(b,yes) to enable \
               colors, $(b,no) to disable them, or $(b,auto) to enable \
               them if the output is a TTY." in
    let e = Arg.enum [ "auto", C_auto ; "yes", C_yes ; "no" , C_no ] in
    Arg.(value & opt e C_auto  & info [ "c"; "colors" ] ~doc)
  in
  let verbosity =
    let doc = "Sets the verbosity level. When called without argument, \
               level is 1 (smallest not null verbosity level), but \
               positional argument(s) $(b,FILE) may need to be \
               separated by \"--\" if no other optional argument is \
               provided after \"-v\"." in
    Arg.(value & opt ~vopt:1 int 0 & info [ "v"; "verbosity"] ~doc) in        
  let debug_mode =
    let doc = "Starts $(b,acgc) in debug mode: it will record and \
               print backtraces of uncaught exceptions." in
    Arg.(value & flag & info [ "d"; "debug" ] ~doc)
  in
  let term_width =
    let doc = "Set the terminal width to be used for pretty printing \
               output. When not set, the value is automatically set to \
               the size of the terminal if the standard ouput is a \
               terminal, or to max_int if the output is a file." in
    Arg.(value & opt (some int) None & info [ "w"; "terminal-width" ] ~doc)
  in
  let man =
    [
      `S Manpage.s_description;
      `P
        "$(tname) parses each file $(i,FILE), which is supposed to be a file \
         containing ACG signatures or lexicons, either as source files \
         (typically with the .acg extension) or object files (with the .acgo \
         extension).";
      `P
        "If all the files are successfully parsed, a binary containing \
         all the ACG signatures and lexicons is created. By default, \
         the name of the generated binay file is \"$(b,FILE)n.acgo\" \
         where $(b,FILE)n.acg is the last parameter (see option -o).";
      `S Manpage.s_bugs;
      `P
        "Report bugs by submitting issues at \
         https://gitlab.inria.fr/ACG/dev/ACGtk/issues.";
      `P "Or report bugs to <sylvain.pogodalla@inria.fr>.";
    ]
  in
  let files = Arg.(non_empty & pos_all string [] & info [] ~docv:"FILE") in
  let exits =
    Cmd.(
      Exit.info
        ~doc:
          "on failure (at least one of the argument files $(b,FILE) does not \
           compile)."
        1
      :: Exit.defaults)
  in
  Cmd.v
    (Cmd.info "acgc" ~version:Version.version ~doc ~exits ~man)
    Term.(const main_function $ term_width $ with_magic $ output_file $ dirs $ files $ colors $ verbosity $ debug_mode)

let () = Cmd.(exit (eval' main))
