%{

    let id_to_term (id,loc) typing_env sg =
      match Data_Signature.is_constant id sg,Typing_env.mem id typing_env with
      | (true,_),false -> Abstract_syntax.Const (id,loc),loc
      | (false,_),true -> Abstract_syntax.Var (id,loc),loc
      | (true,_),true ->
         let sig_name, _ = Data_Signature.name sg in
         let () = Warnings.(issue_warning (Term_parsing (Var_and_const (id, sig_name, loc)))) in
         Abstract_syntax.Var (id,loc),loc
      | (false,_),false -> Errors.(ParsingErrors.emit (Parsing_l.UnknownConstantNorVariable id) ~loc)

    type build_type = Data_Signature.t -> Abstract_syntax.type_def
    type build_term = Typing_env.t -> Data_Signature.t -> Abstract_syntax.term
    type build_token = Typing_env.t -> Data_Signature.t -> Term_sequence.token

    type term0_result =
      | Type_or_token of (build_type * (build_token * Abstract_syntax.location) list * Abstract_syntax.location)

    type type_or_term_result =
      | Type_or_term of (build_type * build_term * Abstract_syntax.location)

    let term0_to_term = function
      | Type_or_token (type_builder,token_builders,loc) ->
         let term_builder type_env sg =
           (*
           let rev_terms =
             List.fold_left
               (fun t_lst (builder,_) ->
                 let t = builder type_env sg in
                 (t::t_lst))
               []
               token_builders in*)
           (*
           let result, _ = Term_sequence.parse_sequence (List.rev rev_terms) sg in *)
           let result, _ = Term_sequence.parse_sequence (List.map (fun (builder, _) -> builder type_env sg) token_builders) sg in
           result in
         Type_or_term (type_builder,term_builder,loc)

    let get_term t type_env sg =
      match t with
        | Type_or_term (_,term_builder,_) -> term_builder type_env sg

    let get_type t sg =
      match t with
        | Type_or_term (type_builder,_,_) -> type_builder sg

%}


%token <Logic.Abstract_syntax.Abstract_syntax.location> ARROW
%token <Logic.Abstract_syntax.Abstract_syntax.location> LIN_ARROW
                                                        (*%on_error_reduce type_expression *)

%token <Logic.Abstract_syntax.Abstract_syntax.location> LAMBDA
%token <Logic.Abstract_syntax.Abstract_syntax.location> LAMBDA0
%token <Logic.Abstract_syntax.Abstract_syntax.location> DOT

%start < AcgData.Signature.Data_Signature.t -> Logic.Lambda.Lambda.term * Logic.Lambda.Lambda.stype > term_alone
%start < AcgData.Signature.Data_Signature.t -> Logic.Lambda.Lambda.stype > type_alone
%on_error_reduce type_or_term(EOI)
%on_error_reduce type_or_term(COLON)
%on_error_reduce type_or_term(SEMICOLON)

%%

%public type_alone:
| ty = type_or_term(EOI) EOI {
    fun sg ->
    let ty = get_type (ty Type_ctx false) sg in
    AcgData.Signature.Data_Signature.convert_type ty sg
  }

%public term_alone:
| t = type_or_term(COLON) COLON ty = type_or_term(EOI) EOI {
    fun sg ->
    let t = get_term (t Term_ctx false) Typing_env.empty sg in
    let ty = get_type (ty Type_ctx false) sg in
    Data_Signature.convert_term t ty sg
  }

%inline arrow :
| l = ARROW { Abstract_syntax.Non_linear,l }
| l = LIN_ARROW { Abstract_syntax.Linear,l }

%public type_or_term(phantom) :
| t = type_or_term0(phantom) { fun k brackets -> term0_to_term (t k brackets) }
| t1 = type_or_term0(phantom) arrow = arrow t2 = type_or_term(phantom) {
    let arrow,arrow_loc =
      match arrow with
      | Abstract_syntax.Linear,loc -> (fun ty1 ty2 l -> Abstract_syntax.Linear_arrow (ty1,ty2,l)),loc
      | Abstract_syntax.Non_linear,loc -> (fun ty1 ty2 l -> Abstract_syntax.Arrow (ty1,ty2,l)), loc in
    fun kind  ->
    if kind = Term_ctx then
      Errors.(ParsingErrors.emit Parsing_l.TermNotArrowExpected ~loc:arrow_loc)
    else
      fun _ ->
      match t1 kind false with
      | Type_or_token (type_builder1,term_builders1,l1) ->
         (match t2 kind false with
          | Type_or_term (type_builder2,_,l2) ->
             let new_loc = new_loc l1 l2 in
             let type_builder sg =
               let ty1' = type_builder1 sg in
               let ty2' = type_builder2  sg in
               let new_type = arrow ty1' ty2' new_loc in
               new_type in
             let () =
               match term_builders1 with
               | (_t_builder1,_)::(_t_builder2,loc2)::_ ->
                  Errors.(ParsingErrors.emit Parsing_l.ArrowExpected ~loc:loc2)
               | _ -> () in
             let term_builder type_env sg =
               let _ =
                 match term_builders1 with
                 | [t_builder,_] -> t_builder type_env sg
                 | _ -> failwith "Bug: Should not happen" in
               Errors.(ParsingErrors.emit Parsing_l.TermNotArrowExpected ~loc:arrow_loc) in
             Type_or_term (type_builder,term_builder,new_loc))
  }
| t = bound_term(phantom) { t }

type_or_term0(phantom):
| id = IDENT {
    fun _ _ ->
    let id,loc = id in
    let type_builder sg =
      match Data_Signature.is_type id sg with
      | true -> Abstract_syntax.Type_atom (id,loc,[])
      | false -> Errors.(ParsingErrors.emit (Parsing_l.UnknownType id) ~loc) in
    let token_builder type_env sg =
      let t, loc = id_to_term (id,loc) type_env sg in
      (*
        match Data_Signature.is_constant id sg,Typing_env.mem id type_env with
        | (true,_),false -> Abstract_syntax.Const (id,loc)
        | (false,_),true -> Abstract_syntax.Var (id,loc)
        | (true,_),true -> Abstract_syntax.Var (id,loc)
        | (false,_),false -> Errors.(ParsingErrors.emit (Parsing_l.UnknownConstantNorVariable id) ~loc) in *)
      Term_sequence.Term (t,loc) in
    Type_or_token (type_builder,[token_builder,loc],loc)
  }
| id = SYMBOL {
    fun kind ->
    let name,loc = id in
    if kind = Type_ctx then
      Errors.(ParsingErrors.emit (Parsing_l.UnknownType name) ~loc)
    else
      fun brackets ->
      let token_builder _type_env sg =
        match Data_Signature.is_constant name sg,brackets with
        | (true,Some (fix, _, false)),false -> Term_sequence.Op (Abstract_syntax.Const (name,loc),fix,loc)
        (* prefix or infix operators are considered as constants when of atomic type *)
        | (true,Some (_fix, _, true)),_
        | (true,Some (_fix, _, _)),true -> Term_sequence.Term (Abstract_syntax.Const (name,loc),loc)
        | (true,None),_ -> failwith "Bug: Should no happen"
        | (false,_),_ -> Errors.(ParsingErrors.emit (Parsing_l.UnknownConstant name) ~loc) in
      let type_builder _ =
        Errors.(ParsingErrors.emit (Parsing_l.UnknownType name) ~loc) in
      Type_or_token (type_builder,[token_builder,loc],loc)
  }
| LPAREN t = type_or_term(phantom) RPAREN {
    fun kind _ ->
    match t kind true with
    | Type_or_term (type_builder,term_builder,l) ->
       let token_builder type_env sg =
         let t = term_builder type_env sg in
         Term_sequence.Term (t,l) in
       Type_or_token (type_builder,[token_builder,l],l)
  }
| id = IDENT t = type_or_term0(phantom) {
    fun kind ->
    match t kind false with
    | Type_or_token (_,token_builders,loc') ->
       let type_builder _ =
         Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:loc') in
       if kind = Type_ctx then
         type_builder ()
       else
         fun _ ->
         let _,loc = id in
         let token_builder type_env sg =
           let cst,loc = id_to_term id type_env sg in
           Term_sequence.Term (cst,loc) in
         Type_or_token (type_builder,(token_builder,loc)::token_builders,new_loc loc loc')
  }
| id = SYMBOL t = type_or_term0(phantom) {
    let name,loc = id in
    fun kind ->
    if kind = Type_ctx then
      Errors.(ParsingErrors.emit (Parsing_l.UnknownType name) ~loc)
    else
      fun _ ->
      let token_builder _type_env sg =
        match Data_Signature.is_constant name sg with
        | true,Some (fix, _, false ) -> Term_sequence.Op (Abstract_syntax.Const (name,loc),fix,loc)
        (* prefix or infix operators are considered as constants when of atomic type *)
        | true,Some (_fix, _, true ) -> Term_sequence.Term (Abstract_syntax.Const (name,loc),loc)
        | true,None -> failwith "Bug: Should no happen"
        | false,_ -> Errors.(ParsingErrors.emit (Parsing_l.UnknownConstant name) ~loc) in
      match t kind false with
      | Type_or_token (_type_builder,token_builders,loc') ->
         let type_builder _ = Errors.(ParsingErrors.emit (Parsing_l.UnknownType name) ~loc) in
         Type_or_token (type_builder,(token_builder,loc)::token_builders,new_loc loc loc')
  }
| LPAREN t = type_or_term(phantom) RPAREN  t0 = type_or_term0(phantom) {
   fun kind ->
   match t0 kind false with
   | Type_or_token (_,token_builders,loc') ->
      (match t kind true with
       | Type_or_term (type_builder,term_builder,l) ->
          if kind = Type_ctx then
            Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:loc')
          else
            let type_builder sg =
              let _ = type_builder sg in
              Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:loc') in
            fun _brackets ->
            let token_builder type_env sg =
              let t = term_builder type_env sg in
              Term_sequence.Term (t,l) in
            Type_or_token (type_builder, (token_builder,l)::token_builders,new_loc l loc'))
  }
