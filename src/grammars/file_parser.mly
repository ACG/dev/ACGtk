 %{

    open UtilsLib
    open Logic.Abstract_syntax
    open AcgData.Environment
    open AcgData.Signature
    open AcgData.Acg_lexicon

    module Typing_env = Utils.StringSet

    let new_loc (s,_) (_,e) = (s,e)

    let get_term_location = function
      | Abstract_syntax.Var (_,l) -> l
      | Abstract_syntax.Const (_,l) -> l
      | Abstract_syntax.Abs (_,_,_,l) -> l
      | Abstract_syntax.LAbs (_,_,_,l) -> l
      | Abstract_syntax.App (_,_,l) -> l


    let abs (x,l1,t) abs_kind =
      let abs_loc = new_loc l1 (get_term_location t) in
      match abs_kind with
      | Abstract_syntax.Linear -> Abstract_syntax.LAbs (x,l1,t, abs_loc), abs_loc
      | Abstract_syntax.Non_linear -> Abstract_syntax.Abs (x,l1,t,abs_loc), abs_loc

    let is_signature s e =
      match Environment.get_opt s e with
      | Some (Environment.Signature _) -> true
      | _ -> false

    let is_lexicon s e =
      match Environment.get_opt s e with
      | Some (Environment.Lexicon _) -> true
      | _ -> false

    let get_sig (name,loc) e = Environment.get_signature name e loc

    let get_lex (name,loc) e = Environment.get_lexicon name e loc

                                           %}

%token EOI
%token <Logic.Abstract_syntax.Abstract_syntax.location> LPAREN
%token <Logic.Abstract_syntax.Abstract_syntax.location> RPAREN
%token <Logic.Abstract_syntax.Abstract_syntax.location> SIG_OPEN
%token <Logic.Abstract_syntax.Abstract_syntax.location> LEX_OPEN
%token <Logic.Abstract_syntax.Abstract_syntax.location> NL_LEX_OPEN
%token <Logic.Abstract_syntax.Abstract_syntax.location> END_OF_DEC
%token <(string*Logic.Abstract_syntax.Abstract_syntax.location)> IDENT
%token <Logic.Abstract_syntax.Abstract_syntax.location> COLON
%token <Logic.Abstract_syntax.Abstract_syntax.location> EQUAL
%token <Logic.Abstract_syntax.Abstract_syntax.location> SEMICOLON

%token <Logic.Abstract_syntax.Abstract_syntax.location> COMPOSE
                                                        (*%right COMPOSE*)


%start <?overwrite:bool -> no_magic:bool -> filename:string -> AcgData.Environment.Environment.t -> AcgData.Environment.Environment.t> main

%%

main:
  | dec=sig_or_lex+ EOI { fun ?(overwrite=false) ~no_magic ~filename e -> List.fold_left (fun acc d -> d ~overwrite ~no_magic ~filename acc) e dec
      }

sig_or_lex:
  | s=signature  { fun ~overwrite ~no_magic:_ ~filename e -> s ~overwrite ~filename e }
  | l=lexicon  { fun ~overwrite ~no_magic ~filename:_ e ->
                 l ~overwrite ~no_magic e }

signature :
  | SIG_OPEN id=IDENT EQUAL entries = end_of_dec (sig_entry)
    {
      fun ~overwrite ~filename e ->
      let s,loc = id in
      if is_signature s e then
       AcgData.Errors.(EnvironmentErrors.emit (Environment_l.DuplicatedEntry s) ~loc)
      else
        let new_sig =
          List.fold_left
            (fun acc entry -> entry acc)
            (Data_Signature.empty ~filename id)
            entries in
        Environment.(insert ~overwrite (Signature new_sig) ~to_be_dumped:true e)
    }

lexicon :
  | LEX_OPEN lex=lex_declaration
    {fun ~overwrite ~no_magic e -> lex ~overwrite ~no_magic ~non_linear:false e }
  | NL_LEX_OPEN lex=lex_declaration
    {fun ~overwrite ~no_magic e -> lex ~overwrite ~no_magic ~non_linear:true e }
  | LEX_OPEN lex=IDENT EQUAL exp=lexicon_exp {fun ~overwrite ~no_magic e ->
                                              let new_lex =
                                                Data_Lexicon.compose_lexicons
                                                  (List.map (fun (l,p) -> get_lex (l,p) e,p) (List.rev exp))
                                                  lex in
                                              let new_lex = if no_magic then new_lex else Data_Lexicon.magic new_lex in
                                              Environment.(insert ~overwrite (Lexicon new_lex) ~to_be_dumped:true e)}



%inline lex_declaration :
  | lex=IDENT LPAREN abs=IDENT RPAREN COLON obj=IDENT EQUAL entries = end_of_dec(lex_entry)
    {fun ~overwrite ~no_magic ~non_linear e ->
     let lex_name,lex_loc = lex in
     let abs',obj'= get_sig abs e,get_sig obj e in
     if is_lexicon lex_name e then
       AcgData.Errors.(EnvironmentErrors.emit (Environment_l.DuplicatedEntry lex_name) ~loc:lex_loc)
     else
       let lex' = List.fold_left
                    (fun acc entry -> entry acc)
                    (Data_Lexicon.empty lex ~abs:abs' ~obj:obj' ~non_linear)
                    entries  in
       let () = Data_Lexicon.check lex' in
       let lex' = if no_magic then lex' else Data_Lexicon.magic lex' in
       Environment.(insert ~overwrite (Lexicon lex') ~to_be_dumped:true e)
    }

end_of_dec(entry_type):
  | entry = entry_type SEMICOLON? END_OF_DEC { [entry] }
  | entry = entry_type SEMICOLON entries = end_of_dec(entry_type) { entry :: entries }


lexicon_exp0 :
  | lex = IDENT { [lex] }
  | LPAREN lex = lexicon_exp RPAREN { lex }

lexicon_exp :
  | lex = lexicon_exp0 { lex }
  | lex1 = lexicon_exp0 COMPOSE lex2 = lexicon_exp
                                         {
                                           lex1 @ lex2
                                         }

