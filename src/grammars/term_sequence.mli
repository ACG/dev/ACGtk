(** This modules implements a cheap term parser to handle infix
    operators associativity and precedence without changing the menhir
    grammar files *)

open Logic.Abstract_syntax
open AcgData.Signature

module Log : UtilsLib.Xlog.MyLOG
(** [Log] is the log module for Term_Sequence *)

type token =
  | Term of (Abstract_syntax.term * Abstract_syntax.location)
  | Op of
      (Abstract_syntax.term
      * Abstract_syntax.syntactic_behavior
      * Abstract_syntax.location)
      (** The type of the tokens. Only [Op] values can have infix
     syntactic properties *)

val parse_sequence :
  token list ->
  Data_Signature.t ->
  Abstract_syntax.term * Abstract_syntax.location
(** [parse_sequence lst sg] returns a pairs consisting of a term and
    its location. The sequence is parsed according to the
    associativity and precedence properties given in [sg]. *)
