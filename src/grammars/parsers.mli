(** This module implements the parsing functions for ACG data files,
    terms, lexical entries, and signature entries *)

open Logic.Lambda
open AcgData.Environment
open AcgData.Signature
open AcgData.Acg_lexicon

val parse_data :
  ?overwrite:bool ->
  ?output:Format.formatter ->
  no_magic:bool ->
  Sedlexing.lexbuf ->
  Environment.t ->
  Environment.t option
(** [parse_data ~override:over ~output:out buf e] parses the
    file [buf]. If the parse is successful, then it returns [Some e']
    where [e'] is [e] with the data from [filenamne added]. Otherwise it
    returns [None]. The two first parameters indicates whether it is allowed
    to overwrite an already present signature or lexicon, and whether
    to ouput the new environment. *)

val parse_term :
  Sedlexing.lexbuf ->
  Data_Signature.t ->
  (Lambda.term * Lambda.stype) option
(** [parse_term buf sg] parses the term contained in the
   buffer [buf] according to the signature [sg] and returns [None] if
   the parse fails and [Some (term,stype)] where [term] and [stype]
   correspond to [buf]. *)

val parse_type :
  Sedlexing.lexbuf ->
  Data_Signature.t ->
  Lambda.stype option
(** [parse_type buf sg] parses the type contained in the
   string [buf] according to the signature [sg] and returns [None] if
   the parse fails and [Some stype] where [stype] correspond to [buf]. *)

val parse_sig_entry :
  Sedlexing.lexbuf ->
  Data_Signature.t ->
  Data_Signature.t option
(** [parse_sig_entry buf sg e] parses the signature entry [buf] and
   adds it to the signature [sg] in the environment [e] and returns
   [None] if parsing fails or [Some sg'] where [sg'] is the new
   signature. *)

val parse_lex_entry :
  Sedlexing.lexbuf ->
  Data_Lexicon.t ->
  Data_Lexicon.t option
(** [parse_lex_entry buf lex e] parses the lexicon entry [buf] and adds it
   to the lexicon [lex] in the environment [e] and returns [None] if
   parsing fails or [Some lex'] where [lex'] is the new lexicon. *)
