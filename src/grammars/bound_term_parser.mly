%on_error_reduce bound_term_ending(EOI)
%on_error_reduce bound_term_ending(COLON)
%on_error_reduce bound_term_ending(SEMICOLON)


%%

  
%public bound_term(phantom) :
   | binder = LAMBDA vars_and_term = bound_term_ending(phantom) 
                                       { fun kind _ -> 
                                         let vars,(bounded_term_builder,t_loc) = vars_and_term in
                                         let n_loc = new_loc binder t_loc in
                                         let type_builder _ =
                                           Errors.(ParsingErrors.emit Parsing_l.TypeExpected ~loc:binder) in
                                         if kind = Type_ctx then
                                           type_builder () 
                                         else
                                           let term_builder type_env sg =
                                             let type_env = List.fold_left
                                                              (fun acc (var,_) -> Typing_env.add var acc)
                                                              type_env
                                                              vars in                                   
                                             let t =
                                               match bounded_term_builder with
                                               | Type_or_term (_,t_builder,_) -> 
                                                  t_builder type_env sg in
                                             (fst
                                                (List.fold_left
                                                   (fun (acc,first_var) (var,loc) ->
                                                     if first_var then
                                                       (fun t -> acc (fst (abs (var,n_loc,t) Abstract_syntax.Non_linear))),false
                                                     else
                                                       (fun t -> acc (fst (abs (var,loc,t) Abstract_syntax.Non_linear))),false)
                                                   ((fun x -> x),true)
                                                   vars)) t in
                                           Type_or_term (type_builder,term_builder,n_loc)
                                       }
| binder = LAMBDA0 vars_and_term = bound_term_ending(phantom)
                                     { fun kind _ -> 
                                         let vars,(bounded_term_builder,t_loc) = vars_and_term in
                                         let n_loc = new_loc binder t_loc in
                                         let type_builder _ =
                                           Errors.(ParsingErrors.emit Parsing_l.TypeExpected ~loc:binder) in
                                         if kind = Type_ctx then
                                           type_builder ()
                                         else
                                         let term_builder type_env sg =
                                           let type_env = List.fold_left
                                                            (fun acc (var,_) -> Typing_env.add var acc)
                                                            type_env
                                                            vars in              
                                           let t =
                                             match bounded_term_builder with
                                             | Type_or_term (_,t_builder,_) -> 
                                                t_builder type_env sg in
                                           (fst
                                              (List.fold_left
                                                 (fun (acc,first_var) (var,loc) ->
                                                   if first_var then
                                                     (fun t -> acc (fst (abs (var,n_loc,t) Abstract_syntax.Linear))),false
                                                   else
                                                     (fun t -> acc (fst (abs (var,loc,t) Abstract_syntax.Linear))),false)
                                                 ((fun x -> x),true)
                                                 vars)) t in
                                         Type_or_term (type_builder,term_builder,n_loc) }
| binder =  IDENT vars_and_term = bound_term_ending(phantom)
                                    {fun kind _ -> 
                                      let binder,loc = binder  in
                                      let vars,(bounded_term_builder,t_loc) = vars_and_term  in
                                      let n_loc = new_loc loc t_loc in
                                      let type_builder sg =
                                        match Data_Signature.is_type binder sg,vars_and_term with
                                        | true,((_,var_loc)::_,_) ->
                                           Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:var_loc)
                                        | true,([],_) -> failwith "Bug: should not happen"
                                        | false,_ -> Errors.(ParsingErrors.emit (Parsing_l.UnknownType binder) ~loc) in
                                      let () =
                                        match kind, vars_and_term with
                                        | Type_ctx,((_,var_loc)::_,_) ->
                                           Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:var_loc)
                                        | Type_ctx,([],_) -> failwith "Bug: Should not happen"
                                        | Term_ctx,_ -> () in
                                      let term_builder type_env sg =
                                        let linearity =
                                          match Data_Signature.is_constant binder sg with
                                          | true,Some (Abstract_syntax.Binder, _, _) ->
                                             (match Data_Signature.get_binder_argument_functional_type binder sg with
                                              | None -> failwith "Bug: Binder of non functional type"
                                              | Some k -> k)
                                          | _ -> Errors.(ParsingErrors.emit (Parsing_l.UnknownBinder binder) ~loc) in
                                        let type_env = List.fold_left
                                                         (fun acc (var,_) -> Typing_env.add var acc)
                                                         type_env
                                                         vars in
                                        let wrap (t,l_t) =
                                          Abstract_syntax.(App (Const (binder, loc), t, new_loc loc l_t)) in
                                        let t =
                                          match bounded_term_builder with
                                          | Type_or_term (_,t_builder,_) -> t_builder type_env sg in
                                        (List.fold_left
                                           (fun acc (var,loc) ->
                                             fun t -> 
                                             let abs_term, abs_term_loc = abs (var,loc,t) linearity in
                                             acc (wrap (abs_term, abs_term_loc)))
                                           (fun x -> x)
                                           vars) t in
                                      Type_or_term (type_builder,term_builder,n_loc) }
| binder =  SYMBOL vars_and_term = bound_term_ending(phantom)
                                    {fun kind _ -> 
                                      let binder,loc = binder  in
                                      let vars,(bounded_term_builder,t_loc) = vars_and_term  in
                                      let n_loc = new_loc loc t_loc in
                                      let type_builder sg =
                                        match Data_Signature.is_type binder sg,vars_and_term with
                                        | true,((_,var_loc)::_,_) ->
                                           Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:var_loc)
                                        | true,([],_) -> failwith "Bug: should not happen"
                                        | false,_ -> Errors.(ParsingErrors.emit (Parsing_l.UnknownType binder) ~loc) in
                                      let () =
                                        match kind, vars_and_term with
                                        | Type_ctx,((_,var_loc)::_,_) ->
                                           Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:var_loc)
                                        | Type_ctx,([],_) -> failwith "Bug: Should not happen"
                                        | Term_ctx,_ -> () in
                                      let term_builder type_env sg =
                                        let linearity =
                                          match Data_Signature.is_constant binder sg with
                                          | true,Some (Abstract_syntax.Binder, _, _) ->
                                             (match Data_Signature.get_binder_argument_functional_type binder sg with
                                              | None -> failwith "Bug: Binder of non functional type"
                                              | Some k -> k)
                                          | _ -> Errors.(ParsingErrors.emit (Parsing_l.UnknownBinder binder) ~loc) in
                                        let type_env = List.fold_left
                                                         (fun acc (var,_) -> Typing_env.add var acc)
                                                         type_env
                                                         vars in
                                        let wrap (t,l_t) =
                                          Abstract_syntax.(App (Const (binder, loc), t, new_loc loc l_t)) in
                                        let t =
                                          match bounded_term_builder with
                                          | Type_or_term (_,t_builder,_) -> t_builder type_env sg in
                                        (List.fold_left
                                           (fun acc (var,loc) ->
                                             fun t -> 
                                             let abs_term, abs_term_loc = abs (var,loc,t) linearity in
                                             acc (wrap (abs_term, abs_term_loc)))
                                           (fun x -> x)
                                           vars) t in
                                      Type_or_term (type_builder,term_builder,n_loc) }
                                    
                                    bound_term_ending(phantom) :
| var = IDENT d = DOT t=type_or_term(phantom) {[var],
                                               let type_builder _ = Errors.(ParsingErrors.emit Parsing_l.ArrowOrRParenthesisExpected ~loc:d) in
                                               match t Term_ctx false with
                                               | Type_or_term (_,term_builder,loc) -> Type_or_term (type_builder,term_builder,loc),loc
                                              }
| var = IDENT vars_and_term = bound_term_ending(phantom) {let vars,t = vars_and_term in var::vars,t}
                                               
