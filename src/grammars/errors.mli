(** This module contains the type for lexing errors in ACGs. *)
module Lexing_l : sig
  type t =
    | UnstartedBracket (** [UnstartedBracket] is raised when an unopened bracket
        is closed. *)
    | MismatchParentheses of char (** [MismatchParentheses c] is raised when
        there is a mismatch with opening and closing of char [c]. *)
    | UnterminatedComment (** [UnterminatedComment] is raised when the end of
        file is reached in an opened comment. *)
    | UnstartedComment (** [UnstartedComment] is raised when the comment closing
        token appear before the opening one. *)
    | BadChar of string (** [BadChar c] is raised when a char [c] can't appear in
        any lexical tokens. *)
    | Malformed (** [Malformed] if raised when an invalid UTF-8 sequence is
        encountered in the input. *)

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module LexingErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Lexing_l.t -> 'a
end

(** This module contains the type for syntax errors in ACGs. *)
module Parsing_l : sig
  type t =
    | MenhirError of int (** [Error i] is raised on a syntax error with [i] the number
        given by Menhir to select the correct message. *)
    | TypeExpected
    | ArrowExpected
    | ArrowOrRParenthesisExpected
    | LessThanExpected
    | AssociativitySpecExpected
    | TermNotArrowExpected
    | InfixMissingFirstArg of string
    | InfixMissingSecondArg of string
    | NotAssociative of string
    | NotInfix of string
    | PrefixMissingArg of string
    | UnknownConstant of string
    | UnknownType of string
    | UnknownBinder of string
    | UnknownConstantNorVariable of string
    | UnknownConstantNorType of string
    | NotDefAsInfix of string
    | DuplicatedTerm of string
    | DuplicatedType of string
    | Other (** [Other] is for an unknown error. *)

  val kind : string
  val pp : Format.formatter -> t -> unit
end

module ParsingErrors : sig
  val emit : ?loc:UtilsLib.Error.pos -> Parsing_l.t -> 'a
end
