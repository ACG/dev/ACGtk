%{


    let add_cst_interpretations term abs_sig lex (id,loc) =
      match Data_Signature.is_constant id abs_sig with
      | true, Some (_, true, _) ->
         (* a macro should not be interpreted *)
         let sig_name, _ = Data_Signature.name abs_sig in
         let lex_name, _ = Data_Lexicon.name lex in
         AcgData.Errors.(LexiconErrors.emit (Lexicon_l.BadInterpretationOfMacro (lex_name, sig_name, id)) ~loc)
      | true, _ -> Data_Lexicon.insert (Abstract_syntax.Constant (id,loc,term)) lex
      | false,_ -> Errors.(ParsingErrors.emit (Parsing_l.UnknownConstant id) ~loc)

   let add_type_interpretation stype abs_sig lex (id,loc) =
     match Data_Signature.is_type id abs_sig with
     | true -> Data_Lexicon.insert (Abstract_syntax.Type (id,loc,stype)) lex
     | false -> Errors.(ParsingErrors.emit (Parsing_l.UnknownType id) ~loc)
   
%}

%token <Logic.Abstract_syntax.Abstract_syntax.location> COLON_EQUAL
%start <AcgData.Acg_lexicon.Data_Lexicon.t -> AcgData.Acg_lexicon.Data_Lexicon.t > lex_entry_eoi
                                                          
%%


  id_or_sym :
| id = IDENT { id }
| sym = SYMBOL { sym }

               lex_entry_eoi :
| e = lex_entry EOI { e }

%public lex_entry :
         
| cst = separated_nonempty_list(COMMA,id_or_sym) COLON_EQUAL t=type_or_term(SEMICOLON)
          {
            fun lex ->
            let abs,obj = Data_Lexicon.get_sig lex in
            let are_types,are_terms =
              List.fold_left
                (fun (are_types,are_terms) (id,loc) ->
                  match Data_Signature.is_type id abs,Data_Signature.is_constant id abs with
                  | false,(false, _) -> Errors.(ParsingErrors.emit (Parsing_l.UnknownConstantNorType id) ~loc)
                  | false,(true, _) when not are_terms -> Errors.(ParsingErrors.emit (Parsing_l.UnknownType id) ~loc)
                  | false,(true, _) -> false,true
                  | true,(false, _) when not are_types -> Errors.(ParsingErrors.emit (Parsing_l.UnknownConstant id) ~loc)
                  | true,(false, _) -> true,false
                  | true,(true,_) -> true && are_types,true && are_terms)
                (true,true)
                cst in
            match are_types,are_terms with
            | false,false -> failwith "Bug: Should not happen"
            | true,true ->
               (match t Type_ctx false with
                | Type_or_term (type_builder, term_builder,_) ->
                   let term = term_builder Typing_env.empty obj in
                   let stype = type_builder obj in
                   List.fold_left
                     (fun acc cst ->
                       add_type_interpretation
                         stype
                         abs
                         (add_cst_interpretations term abs acc cst)
                         cst)
                     lex
                     cst)
            | false,true ->
               (match t Term_ctx false with
                | Type_or_term (_, term_builder,_) ->
                   let term = term_builder Typing_env.empty obj in
                   List.fold_left (fun acc cst -> add_cst_interpretations term abs acc cst) lex cst)
            | true,false ->
               (match t Type_ctx false with
                | Type_or_term (type_builder, _,_) ->
                   let stype = type_builder obj in
                   List.fold_left (add_type_interpretation stype abs) lex cst)
                }
