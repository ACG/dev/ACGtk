(** This modules implements the cheapest interface to allow for having
    an acg version that behaves correctly, whether the optional
    dependency cairo2 is available or not *)

(** The abstract type for the svg config *)
type config

(** [has_cairo] is [true] if cairo2 was available at build time and [false] otherwise. *)
val has_cairo : bool

(** [get config ~filename ~dirs] returns a config file made of the
    json file specified with [filename] and [dirs]. If the former is
    [None], returns a default configuration. *)
val get_config : filename:string option -> dirs:string list -> config

val dump_svg : svg_output:string -> 
               AcgData.Signature.Data_Signature.term -> 
               index:int ->
               AcgData.Acg_lexicon.Data_Lexicon.t list -> config:config -> unit
