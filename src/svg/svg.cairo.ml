type config = Svg_rendering.Rendering_config.config

let has_cairo = true

let get_config ~filename ~dirs =
  match filename with
  | None -> Svg_rendering.Svg.default_config
  | Some filename -> Svg_rendering.Svg.get_config filename dirs

let dump_svg ~svg_output:filename term ~index lexlist ~config =
  if filename <> "" then
    let d = Svg_rendering.Svg.realize_diagram term lexlist config in
    let filename =
      match index with
      | -1 -> filename
      | _ ->
         Printf.sprintf
           "%s%i%s"
           (Filename.remove_extension filename)
           (index + 1)
           (Filename.extension filename) in
    Svg_rendering.Svg.to_svg ~filename d
  else
    ()
