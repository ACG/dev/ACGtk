open UtilsLib

type config = unit

let has_cairo = false

let get_config ~filename:_ ~dirs:_ = ()

let dump_svg ~svg_output:_ _ ~index:_ _ ~config:_ =
  Logs.warn
    (fun m -> m
                "The@ %a@ command@ sets@ a@ svg@ output@ file@, but@ \
                 the@ %a opam@ package@ is@ not@ installed.@ No@ svg@ \
                 can@ be@ produced.@ This@ option@ is@ ignored."
                Utils.fun_pp
                "realize"
                Utils.bold_pp
                "cairo2")
