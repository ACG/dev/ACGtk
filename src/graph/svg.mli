val realize_diagram :
  AcgData.Signature.Data_Signature.term -> AcgData.Acg_lexicon.Data_Lexicon.t list -> Rendering_config.config -> Diagram.diagram

val to_svg : filename:string -> Diagram.diagram -> unit
val get_config : string -> string list -> Rendering_config.config
val default_config : Rendering_config.config
