open Diagram
open Logic.Lambda.Lambda
open Show_exts

module Lambda_show (_ : Show_text_sig) : sig
  val fix : (('a -> 'b) -> 'a -> 'b) -> 'a -> 'b
  val parenthesize_d : diagram * bool -> diagram
  val term_to_diagram_open : open_pp
  val term_to_diagram : term -> consts -> diagram
end

module Make
    (_ : Show_text_sig)
    (_ : Show_colors_sig)
    (_ : Show_embellish_sig) : sig
  type lexicon = AcgData.Acg_lexicon.Data_Lexicon.t
  type term = AcgData.Signature.Data_Signature.term

  val realize_diagram :
    term -> lexicon list -> Rendering_config.config -> diagram
end
