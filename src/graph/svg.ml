module ShowI = Show.Make(Show_text_dejavu)
  (Show_colors_solarized_dark)
  (Show_embellish_examples.Make(Show_text_dejavu))

let realize_diagram = ShowI.realize_diagram
let to_svg ~filename d = Diagram.to_svg filename d

let get_config conf dirs = Rendering_config.get_config conf dirs

let default_config = Rendering_config.default
