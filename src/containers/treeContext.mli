(** This module implements trees and tre contexts (i.e., zippers).  *)

(** The module for trees *)
module Tree :
sig
  type 'a tree = Node of ('a * 'a tree list)
                         
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a tree -> unit
  (** [pp ppf fmt t] pretty prints the tree [t] on the formatter [fmt]
     using the pretty printer for tree nodes [ppf] *)

  val fold_depth_first : ('a -> 'b) * ('b -> 'b -> 'b) -> 'a tree -> 'b
  (** [fold_depth_first (f,g) t] recursively computes [(g a) b_1
     .... b_n] where [a=f t_0] and [b_i= f t_i] and [t] is a tree of
     node [t_0] and of children [t_1...t_n]*)

  val label : 'a tree -> 'a
end

(** The module for tree contexts *)
module TreeContext :
sig
  type 'a t =
    | Top
    | Zipper of ('a * 'a Tree.tree ListContext.focused_list * 'a t)
    (** The type of the context. Top is the empty context *)
  
  type 'a focused_tree = 'a t * 'a Tree.tree
  (** The type for focused trees (i.e., trees with their context *)

  val up : 'a focused_tree -> 'a focused_tree
  (** [up (z,t)] moves up in the context [z] of [t] and returns the
     new focused tree [(z',t')] *)

end
  
    
