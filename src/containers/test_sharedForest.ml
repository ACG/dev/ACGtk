open Containers
open SharedForest.SharedForest

let init_f_list l = Forest l

let tree0 =
  Node
    ( "tree0.(0,1)",
      [
        init_f_list
          [
            Node ("tree0.(0,1).(1,1)", []);
            Node ("tree0.(0,1).(1,2)", []);
            Node ("tree0.(0,1).(1,3)", []);
          ];
        init_f_list
          [
            Node ("tree0.(0,1).(2,1)", []);
            Node ("tree0.(0,1).(2,2)", []);
            Node ("tree0.(0,1).(2,3)", []);
          ];
      ] )

let tree =
  Node
    ( "tree.(0,1)",
      [
        init_f_list
          [
            Node ("(0,1).(1,1)", []);
            Node ("(0,1).(1,2)", []);
            Node ("(0,1).(1,3)", []);
          ];
        init_f_list
          [
            Node
              ( "(0,1).(2,1)",
                [
                  init_f_list
                    [
                      Node ("(0,1).(2,1),(1,1)", []);
                      Node ("(0,1).(2,1),(1,2)", []);
                    ];
                  init_f_list [ Node ("(0,1).(2,1),(2,1)", []) ];
                  (*                  Link_to (1, [ (1, 2) ]) (*				  Link_to (2,[(1,2)]);  *); *)
                  Link_to (2, [ (2, 1) ]) (*				  Link_to (2,[(1,2)]);  *);
                ] );
            Node ("(0,1).(2,2)", []);
          ];
        init_f_list
          [
            Node ("(0,1).(3,1)", []);
            Node ("(0,1).(3,2)", []);
            Node ("(0,1).(3,3)", []);
            Node ("(0,1).(3,4)", []);
          ];
        init_f_list [ Node ("(0,1).(4,1)", []); tree0 ];
      ] )

let tree1 =
  [
    Node ("2", [ Link_to (1, []) ]);
    Node ("1", [ init_f_list [ Node ("2.1", []) ] ]);
    tree0;
    tree
  ]

type inputs = Stop | Next | All

let return_input s =
  match String.lowercase_ascii (String.trim s) with
  | "y" | "yes" -> Some Next
  | "n" | "no" -> Some Stop
  | "a" | "all" -> Some All
  | "" -> Some Next
  | _ -> None

let interact_aux get_input = get_input (read_line ())

let rec interact message get_input =
  let () = Printf.printf "%s %!" message in
  match interact_aux get_input with
  | Some v -> v
  | None -> interact message get_input

let rec ask_for_next_parse ?(interactive = true) f param limit =
  let rec all_results l_par =
    match f l_par with
    | None -> Printf.printf "No other returned value\n"
    | Some new_par -> all_results new_par
  in
  match interactive with
  | true -> (
      let msg =
        Printf.sprintf
          "Do you want to look for another solution?\n\
           \ty/yes\n\
           \tn/no\n\
           \ta/all\n\
           (Default: yes):"
      in
      match interact msg return_input with
      | Next -> (
          let () = Printf.printf "Going to get a term\n%!" in
          match f param with
          | None -> Printf.printf "No other returned value\n"
          | Some new_param -> ask_for_next_parse f new_param limit)
      | All -> all_results param
      | Stop -> ())
  | false when snd param <= limit -> (
      let () = Printf.printf "Going to get a term\n%!" in
      match f param with
      | None -> Printf.printf "No other returned value\n"
      | Some new_param -> ask_for_next_parse ~interactive f new_param limit)
  | false -> ()

let () =
  let () = UtilsLib.Utils.resize_terminal ~width:None in

  let () = UtilsLib.Xlog.set_level ~app:"test_Sharedforest" Logs.Warning in
  let () = Logs.app (fun m -> m "Dealing with forest:@ @[%a@]" (pp_forest Format.pp_print_string) tree1) in
  let resumptions = init ~alt_max: 5(Format.pp_print_string) tree1 in
  let () = Logs.app (fun m -> m "**********************************") in
  ask_for_next_parse ~interactive:false
    (fun (res, i) ->
      match resume res with
        | None, _-> None
        | Some (t, _size), resume ->
          let () = Logs.app (fun m -> m "Got result %i" i) in
          let () = Logs.app (fun m -> m "%a" (TreeContext.Tree.pp Format.pp_print_string) t) in
          Some (resume, i + 1))
    (resumptions, 1)
    5000
