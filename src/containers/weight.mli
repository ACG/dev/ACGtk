(** This module implements different weighting schemes to be used for
   sorting computational states (in our case, trees as described by
   traversing focused shared forests). *)

module type Weight_sig =
sig
  type w
  (** The type of weight *)
  
  val pp : Format.formatter -> w -> unit
  (** [pp fmt w] pretty prints the weight [w] on the formatter
     [fmt]. *)
  
  val init : w
  (** an initial value for the weight (somehow a unit of the ring of
      weights) *)
    
  val is_better : w -> w -> bool
  (** [is better w1 w2] returns [true] if [w1] is strictly better (in
     the sense implemented by the module) than [w2] *)

  val is_equal : w -> w -> bool
  (** [is better w1 w2] returns [true] if [w1 = w2] *)
  
  val up : w -> 'a -> w
  (** [up w v] returns the new weight computed from [w] and from [v]
     (the label of the current node, before moving up in the tree). *)
  
  val down : w -> 'a -> w
  (** [down w v] returns the new weight computed from [w] and from [v]
     (the label of the current node, before moving down in the
     tree). *)
    
  val right : w -> 'a -> w
  (** [right w v] returns the new weight computed from [w] and from
     [v] (the label {e parent} node of the current node. *)

  (** This module implements a map from weights to lists of
     computational states of same weight. *)
  module WMap : 
  sig
    type 'a t
    (** The type of the map *)
        
    val empty : 'a t
    (** [empty] returns the empty map *)
        
    val optimum : 'a t -> w option
    (** [optimum m] returns [None] if [m] is empty and [Some w] if [w]
       is the best weight of the current computations stored in the
       map [m]. *)
    
    val pp : Format.formatter -> 'a t -> unit
    (** [pp fmt m] pretty prints (a summary of) the map [m] on the
       formatter [fmt]. *)
      
    val add : w -> 'a -> 'a t -> 'a t
    (** [add w e m] returns [m] where [e] was added with weight
       [w]. *)

    val pop_optimum : 'a t -> ('a * w * 'a t) option
    (** [pop_optimum m] returns [None] if [m] is empty and [Some (c,
       w, m')] where [w] is the best weight of the current
       computations stored in the map [m], [c] is a computation with
       this weight, and [m'] is [m] where [c] was removed. *)
  end
end

(** A module that implements weight as the depth of a tree (the number
   of nodes does not matter. The smaller, the better *)
module Weight_as_Depth : Weight_sig

(** A module that implements weight as the the lexicographic order
   over (depth, size) of a tree (where size is the number of
   nodes). The smaller the better. *)
module Weight_as_Depth_and_Size : Weight_sig
