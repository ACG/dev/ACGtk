(** This module implements resumptions (computational states that a
   program reaches or from which it can start.  *)

module type Resumptions_sig =
  sig
    type 'a resumptions
    (** The type of the resumption store *)
        
    type 'a computation
    (** The type of the computation, ['a] being the type of the
       computational state to be stored *)

    type w
    (** The type of the weight of the computational states *)
    
    val empty : alt_max:int -> 'a resumptions
    (** [empty ~alt_max] returns an empty resumption store. [alt_max]
       defines the maximal number of states to be stored while keeping
       sorted according to [w]. *)
        
    val regular_sorting : 'a resumptions -> bool
    (** [regular_sorting r] returns [true] if [r] still uses regular
       sorting and [false] otherwise. *)
      
    val is_empty : 'a resumptions -> bool
    (** [is_empty r] returns [true] if [r] contains no computational
       states. *)
      
    val extend_resumptions : computation:'a computation ->
      weight:w -> 'a resumptions -> 'a resumptions
    (** [extend_resumptions ~computation ~weight r] returns [r] to
       which the computational state [computation] with weight
       [weight] was added. *)
        
    val pp : Format.formatter -> 'a resumptions -> unit
    (** [pp fmt r] pretty prints the resumption store [r] on the
       formatter [fmt]. *)
      
    val swap: ?current_computation:'a computation * w ->
      'a resumptions -> 'a computation * w * 'a resumptions
    (** [swap ~current_computation r] returns [(c,w,r')] where [r'] is
       [r] to which [current_computation] was added and [c], whose
       weight is [w], was removed. If [r] is such that regular sorting
       is disabled, [r'] is [r] and [(c,w)] is
       [current_computation]. *)
  end

(** This module is a functore generating a resumption store for the
   specified weight module and the specified computational type *)
module Make
    (W : Weight.Weight_sig)
    (C:sig type 'a computation end) : Resumptions_sig with
  type 'a computation = 'a C. computation and type w = W.w
