(**
   This module implements list contexts.
*)

type 'a t
(** The type of the list contexts *)
    
val empty : 'a t
(** [empty] returns an empty context *)

val is_empty : 'a t -> bool
(** [is_empty z] returns true if the context [z] is the empty context
   *)

val push : 'a -> 'a t -> 'a t
(** [push elt z] returns the context [z] to which [elt] was added *)
    
val size : 'a t -> int
(** [size z] returns the size (i.e., the number of elements) of [z] *)
  
type 'a focused_list = 'a t * 'a list
(** The type for focused lists, i.e., lists with there context *)
                         
type direction = Right | Left
(** The type for directions in which we can move in a focused list *)
                 
exception Move_failure of direction
(** Exceptions raised when moving on some direction in a focused list
   is not possible: to [Right] when the list under focus is empty, to
   [Left] when the context of the list is empty. *)
    
val forward: ?step:int -> 'a focused_list -> 'a focused_list
(** [forward ~step f_l] moves forward (if [step] is greater than 0) or
   backward (if [step] is less than 0) in [f_l]. [step] defaults to
   1. *)
    
val right: 'a focused_list -> 'a focused_list
(** [right f_l] moves forward (i.e., right) in the focused list. It is
   the same as [forward f_l] *)
    
val left: 'a focused_list -> 'a focused_list
(** [left f_l] moves backward (i.e., left) in the focused list. It is
   the same as [forward ~step:(-1) f_l] *)
    
val zip_up : 'a t -> 'a list -> 'a list
(** [zip_up f_l] moves left in the focused list [f_l] until the
   context is empty and returns the list to which it is the context *)
    
val fold : 'a focused_list -> ('a focused_list -> 'b -> 'b) -> 'b -> 'b
(** [fold (c, l) f acc] runs [ f ... f (right (right (c, l))) (f
     (right (c, l)) (f (c, l) acc)) ] until no move to right is
     allows.  *)
  
val full_left : 'a focused_list -> 'a focused_list
(** [full_left f_c] returns the same focused list as [(empty, zip_up
   f_l)]. *)
    
val forward_insert : 'a -> 'a focused_list -> 'a focused_list
(** [forward_insert elt (z, l)] returns [(z, elt :: l)], i.e., the
   list [l] to which [elt] was added, in the same context as [l]. *)

val backward_insert : 'a -> 'a focused_list -> 'a focused_list
(** [backward_insert elt (z, l)] returns [(push elt z, l)], i.e., the
   list [l] in the context [z] to which [elt] was added. *)
                                    
val nth_context : int -> 'a list -> ('a focused_list * 'a)
(** [nth_context i [a_1; …; a_i; …; a_n]] returns a pair made of the
   focused list [(z, [a_i+1 …; a_n])] and [a_i] such that [[a_1; ….;
   a_n] = zip_up (z, ([a_i; a_i+1; …; a_n])) = zip_up (forward_insert
   a_i (z, [a_i+1 …; a_n]))]. Raise [Move_failure Right] if [i] is
   strictly greater than [n].*)
