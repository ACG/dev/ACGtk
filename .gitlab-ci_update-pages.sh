#!/bin/sh

set -e
set -x

# Getting the last "page" successful job
page=1
while true; do
	# Getting last pipeline id
	pipeline_id=$(curl "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/pipelines?per_page=1&page=$page" | jq '.[] .id')

	job="$(curl "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/pipelines/$pipeline_id/jobs")"

	# Getting its "page" job status
	job_status="$(curl "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/pipelines/$pipeline_id/jobs" | jq '.[] | select(.name=="pages") .status')"
	
	if [ "$job_status" = '"success"' ]; then
		# Getting its "page" job id
		job_id="$(curl "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/pipelines/$pipeline_id/jobs" | jq '.[] | select(.name=="pages") .id')"
		break
	else
		page=$((page+1))
	fi
done

# Downloading last pages artifact
curl --location --output pages.zip "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/jobs/$job_id/artifacts"

unzip -o pages.zip || true

vername="$CI_COMMIT_BRANCH-$(acg --version)/"
dirname="public/$vername"

rm -rf "$dirname"
mkdir -p "$dirname"
cp -r _build/default/_doc/_html/* "$dirname"

if [ "$CI_COMMIT_BRANCH" = release ]; then
	rm -rf "public/release"
	mkdir -p "public/release"
	cp -r _build/default/_doc/_html/* "public/release"
fi

cat > public/index.html <<EOF
<!DOCTYPE html>
<html lang=fr>
	<head>
		<meta charset=utf-8>
		<title>ACGtk documentation</title>
	</head>
	<body>
EOF

if [ -d public/release ]; then
		cat >> public/index.html <<EOF
		<p>
			<a href="release">ACGtk latest release</a>
		</p>
EOF
fi

for dir in $(ls public); do
	if [ -d "public/$dir" -a "$dir" != release ]; then
		cat >> public/index.html <<EOF
		<p>
			<a href="$dir">ACGtk version $dir</a>
		</p>
EOF
	fi
done

cat >> public/index.html <<EOF
	</body>
</html>
EOF
