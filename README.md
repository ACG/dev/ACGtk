***
ACG development toolkit

Copyright 2008-2024 INRIA

More information on "https://acg.loria.fr/"
License: CeCILL, see the LICENSE file or "http://www.cecill.info"
Authors: see the AUTHORS file
***

# ACGtk: an ACG development toolkit

**ACGtk** is a software package ([2008-2024 INRIA](http://www.inria.fr)©) for the development of abstract categorial grammars. This distribution provides two executables: `acgc` and `acg`.

It is distributed with the *CeCILL* license (see the [LICENSE](LICENSE.en) file or http://www.cecill.info). Contributors are listed in the [AUTHORS.md](AUTHORS.md) file.

A list of related publications is available at the [ACG web page](https://acg.loria.fr).


## acgc

`acgc` is a "compiler" of ACG source code, i.e., files containing definitions of signatures and lexicons. It basically checks whether they are correctly written (syntactically and w.r.t. types and constant typing) and outputs an `.acgo` object file.

Run
```bash
./acgc --help
```
to get help.

## acg

`acg` is an interpreter of command meant to be useful when using ACGs. To get a list of command, run
```bash
./acg
```
then, on the prompt, type
```
ACGtk> help
```

Example files are given in the [examples directory](examples). Read the [README.md file](examples/README.md).

More [detailed documentation](https://acg.gitlabpages.inria.fr/dev/ACGtk/release/) is available for [users](https://acg.gitlabpages.inria.fr/dev/ACGtk/release/acgtk/users.html) and [developers](https://acg.gitlabpages.inria.fr/dev/ACGtk/release/acgtk/developers.html).

# Support

ACGtk development was supported:
+ By the *Langues, Connaissances & Humanités Numériques* CPER (*contrat de plan État-Région*), funded by the **Ministère de l'Enseignement Supérieur et de la Recherche**, the **Région Lorraine**, and the **Fonds européen de développement régional** pour la région Lorraine puis Grand Est (from April 2022 to July 2022).

	[<img alt="Ministère de l'Enseignement Supérieur et de la Recherche" src="logos/MESR.svg" height="120"/>](https://www.enseignementsup-recherche.gouv.fr/fr)&emsp;&emsp;&emsp;
	[<img alt="Région" src="logos/Region-Lorraine.jpeg" height="60"/>](https://www.grandest.fr/)&emsp;&emsp;&emsp;
	[<img alt="Be Europe en Grand Est" src="logos/Feder-Grand-Est.svg" height="120"/>](https://beeurope.grandest.fr/)

+ By [<img alt="Inria" src="logos/inr_logo_rouge.svg" height="30"/>](https://www.inria.fr/) (ADT program, from August 2022 to July 2024).
  
  
