load "montague.acg";

(* We first show that the term "LOVE MARY JOHN"
   that is built in the "syntax" signature
   is indeed of type s *)
check signature=syntax < "LOVE MARY JOHN : s";

(* And, for instance, that it is not of type np *)
check signature=syntax < "LOVE MARY JOHN : np";

(* It is also possible to list all terms of a given type *)
list-terms signature=syntax type=s;

(* How is that term realized as a string?
   Just ask: *)
realize lexicons=syntactic_realisation < "LOVE MARY JOHN : s";

(* And how is it realised semantically? *)
realize lexicons=semantics < "LOVE MARY JOHN : s";

(* Now let's check the realisation and the meaning of
   (EVERY MAN) (lambda x. (SOME WOMAN) (lambda y. LOVE y x)) 
   which of course is of type s; *)
realize lexicons=syntactic_realisation, semantics < "(EVERY MAN) (lambda x. (SOME WOMAN) (lambda y. LOVE y x)) : s";

(* And the one of
   (SOME WOMAN) (lambda y. (EVERY MAN) (lambda x. LOVE y x))
   which is also of type s; *)
realize lexicons=syntactic_realisation, semantics < "(SOME WOMAN) (lambda y. (EVERY MAN) (lambda x. LOVE y x)) : s";

(* Indeed, these two different syntactic terms have the same syntactic
   realisation but two different meanings *)

