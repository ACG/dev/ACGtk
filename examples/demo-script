(* This document more or less reflects the examples that could have
 been seen during the acgtk demo at the Forum Sciences Cognitives 2015
 in Nancy and other examples that were developed short after. *)


(* We start with the Montague examples. *)
load "montague-fr.acg";

(*  Something to draw attention: connecting language and logic in a *)
(*  non-trivial way. *)
realize lexicons=realisation_francais, semantique < "CHAQUE HOMME (AIMER MARIE) : s";
 
(*  Starting simple: viewing lexical items as functions. *)
realize lexicons=realisation_francais < "AIMER MARIE JEAN : s";

(*  Deriving the semantics in parallel. *)
realize lexicons=realisation_francais, semantique < "AIMER MARIE JEAN : s";

(*  And back to our initial example with the quantifier. *)
realize lexicons=realisation_francais, semantique < "CHAQUE HOMME (AIMER MARIE) : s";

(*  Making it more complicated: two quantifiers. *)
realize lexicons=realisation_francais, semantique < "CHAQUE HOMME (lambda x. UNE FEMME (lambda y. AIMER y x)) : s";

(*  Generating a different reading of the same sentence. *)
realize lexicons=realisation_francais, semantique < "UNE FEMME (lambda y. CHAQUE HOMME (lambda x. AIMER y x)) : s";

(*  We can translate not only to the French language and to the logical *)
(*  language, but also to the English language. *)
realize lexicons=realisation_francais, semantique, realisation_anglais < "CHAQUE HOMME (lambda x. UNE FEMME (lambda y. AIMER y x)) : s";

(*  However, the translations we have seen so far have been word-to-word. We *)
(*  can also try something more elaborate: translation on the level of *)
(*  constructions. *)
realize lexicons=realisation_francais, semantique, realisation_anglais < "CHAQUE HOMME (AIMER (POSS_F MARIE MÈRE)) : s";

(*  And finally, this is here to just show that thanks to compositionality, *)
(*  we can scale up to more and more elaborate examples. *)
realize lexicons=realisation_francais, semantique, realisation_anglais < "CHAQUE HOMME (lambda x. UNE FEMME (lambda y. AIMER (POSS_F y MÈRE) x)) : s";

(*  During the demo, we also developed control verbs such as "vouloir". *)
realize lexicons=realisation_francais, semantique < "VOULOIR_CV (AIMER MARIE) JEAN : s";

(*  "vouloir" used as a verb expecting a sentential complement. *)
realize lexicons=realisation_francais, semantique < "VOULOIR_SC (QUE (AIMER MARIE JEAN)) JEAN : s";

 

(*  Now we look at the TAG examples. *)
load "tag-fr.acg";

(*  Let's look at the TAG treatment of a simple sentence. *)
realize lexicons=tag_syntax, tag_yields, tag_semantics < "C_chasse I_s I_vp (C_chien C_un (C_grand (C_noir I_n))) (C_chat C_un I_n) : S";

(*  Since TAG is 2nd-order, we can enjoy the benefits of parsing. *)
parse lexicon=tag_yields type=S < "lambda z. un(grand(chien(noir(chasse(un(chat(z)))))))  : string";

(*  The parse also offers us an alternative syntactic structure (difference *)
(*  in the order of adjunctions corresponding to the left adjective "grand" and *)
(*  the right adjective "noir". *)
realize lexicons=tag_syntax, tag_yields, tag_semantics < "C_chasse I_s I_vp (C_chien C_un (C_noir (C_grand I_n))) (C_chat C_un I_n) : S";

(*  Zooming into the NP "un grand chien noir" to look at details. *)
realize lexicons=tag_syntax, tag_yields, tag_semantics < "C_chien C_un (C_grand (C_noir I_n)) : N";


(*  A bizarre dadaesque example to showcase the semantics on more complicated *)
(*  sentences. *)

parse lexicon=tag_yields type=S < "lambda z. chaque(chien(noir(affirme(que(marie(aime(francois(z))))))))  : string";
realize lexicons=tag_syntax, tag_yields, tag_semantics < "C_aime (C_affirme_que I_s I_vp (C_chien C_chaque (C_noir I_n))) I_vp C_marie C_francois : S";


(*  Trying out some of the complex examples from tag-script in the French *)
(*  grammar. *)

parse lexicon=tag_yields type=S < "lambda z. qui(est(ce(que(marie(aime(z))))))  : string";
realize lexicons=tag_syntax, tag_yields, tag_semantics < "C_est_ce_que_aime I_s I_vp C_qui C_marie : S";

parse lexicon=tag_yields type=S < "lambda z. est(ce(que(marie(croit(que(jean(aime(paul(z)))))))))  : string";
realize lexicons=tag_syntax, tag_yields, tag_semantics < "C_aime (C_est_ce_que_croit_que I_s I_vp C_marie) I_vp C_jean C_paul : S";

parse lexicon=tag_yields type=S < "lambda z. qui(est(ce(que(paul(affirme(que(jean(a(dit(que(francois(aime(z)))))))))))))  : string";
realize lexicons=tag_syntax, tag_yields, tag_semantics < "C_est_ce_que_aime (C_a_dit_que (C_affirme_que I_s I_vp C_paul) I_vp C_jean) I_vp C_qui C_francois : S";

